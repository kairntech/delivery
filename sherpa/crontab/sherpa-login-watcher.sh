#!/bin/bash
#0 * * * * /opt/monitor-sherpa/sherpa-login-watcher.sh 1>>/opt/monitor-sherpa/.log/sherpa-login-watcher.out 2>>/opt/monitor-sherpa/.log/sherpa-login-watcher.err

ROOT=/opt/monitor-sherpa
FILE_LOGINS_SHERPA=${ROOT}/.nblogins-sherpa
WHITELISTED_USERS="admin gkarcher bfreuden hlafayette vnibart olivier oterrier gpouzenc stefan sgeissler gmeulenbelt opendemo"

MAILTPL=${ROOT}/mailtosend.login.html
MAILFROM="info@kairntech.com"
PLATFORM="KAIRNTECH instance $( hostname ).kairntech.com"

# Start
echo "***** $( date ) Starting *****"
OLDIFS=$IFS
IFS=$'\n'

# Get number of Logins
STRING_TO_MATCH='log in'

# Control on sherpa-core
if [ ! -f "${FILE_LOGINS_SHERPA}" ] ; then
  echo "0" > "${FILE_LOGINS_SHERPA}"
fi
NB_LOGINS_SHERPAH=$( cat "${FILE_LOGINS_SHERPA}" )
LOGINS_SHERPA=$( docker exec sherpa-core cat /var/log/sherpa/sherpa.log 2>/dev/null | grep "$STRING_TO_MATCH" )
NB_LOGINS_SHERPA=$( echo "${LOGINS_SHERPA}" | grep -c "$STRING_TO_MATCH" )

# Finalizing
LOGIN_SHERPA=false
NB_LOGINS_SHERPADIFF=0
if [ "${NB_LOGINS_SHERPA}" -ne "${NB_LOGINS_SHERPAH}" ] ; then
  if [ "${NB_LOGINS_SHERPA}" -ne 0 ] ; then
    NB_LOGINS_SHERPADIFF=$(( NB_LOGINS_SHERPA - NB_LOGINS_SHERPAH ))
    if [ ${NB_LOGINS_SHERPADIFF} -gt 0 ] ; then
      echo "${NB_LOGINS_SHERPA}" > ${FILE_LOGINS_SHERPA}
      echo " - Looks like I have to send an email: login detected"
      LOGIN_SHERPA=true
    elif [ ${NB_LOGINS_SHERPADIFF} -lt 0 ] ; then
      echo "${NB_LOGINS_SHERPA}" > ${FILE_LOGINS_SHERPA}
      echo " - Looks like sherpa-core has been restarted"
      echo " - Looks like I have to send an email: login detected"
      NB_LOGINS_SHERPADIFF=${NB_LOGINS_SHERPA}
      LOGIN_SHERPA=true
    fi
  fi
fi

NEWLINES=''
if [ "$LOGIN_SHERPA" == "true" ] ; then
  DATENOW=$( date )
  cp ${MAILTPL} ${MAILTPL}.ts
  sed -i "s|MAILFROM|$MAILFROM|g" ${MAILTPL}.ts
  sed -i "s|DATE|$DATENOW|g" ${MAILTPL}.ts
  sed -i "s|PLATFORM|$PLATFORM|g" ${MAILTPL}.ts

  LOGINS_SHERPA=$( docker exec sherpa-core cat /var/log/sherpa/sherpa.log 2>/dev/null | grep "$STRING_TO_MATCH" | tail -n "$NB_LOGINS_SHERPADIFF" )
  while IFS= read -r s; do
    LINE=$( echo "$s"|sed -e 's/^[ \t]*//' -e 's/[ \t]*$//' )
    ISSTREAMLIT=$( echo "${LINE}"|grep -c "SecurityAPI" )
    LOGINUSER=$( echo "${LINE}"|cut -d "'" -f2 )
    LOGINGROUP=$( echo "${LINE}"|cut -d "'" -f4 )
    TIMESTAMP=$( echo "${LINE}"|cut -d "'" -f1 )
    TIMESTAMP_DATE=$( echo "${TIMESTAMP}"|cut -d " " -f1 )
    TIMESTAMP_HOUR=$( echo "${TIMESTAMP}"|cut -d " " -f2 )
    TOINSERT=$( echo "${WHITELISTED_USERS}" | grep -c "${LOGINUSER}" )
    if [ "${TOINSERT}" -eq 0 ] ; then
      if [ "${ISSTREAMLIT}" -eq 1 ] ; then
        NEWLINE="<li style='display:list-item;'><font style='color:darkblue;'>[${TIMESTAMP_DATE} ${TIMESTAMP_HOUR}]</font>: <font style='color:green; font-weight:bold;'>${LOGINUSER}</font> <font style='color:green; font-style:italic'>(${LOGINGROUP})</font> -via Sherpa API-</li>"
      else
        NEWLINE="<li style='display:list-item;'><font style='color:darkblue;'>[${TIMESTAMP_DATE} ${TIMESTAMP_HOUR}]</font>: <font style='color:green; font-weight:bold;'>${LOGINUSER}</font> <font style='color:green; font-style:italic'>(${LOGINGROUP})</font></li>"
      fi
      echo "${NEWLINE}" >> ${MAILTPL}.ts
      NEWLINES+=${NEWLINE}
    fi
  done < <(echo "${LOGINS_SHERPA}")

  {
    echo "    </ul>"
    echo "    </p>"
    echo "    <p>Current date/time is <font style='color:blue;'>${DATENOW}</font>.<br/></p>"
    echo "    <p>&nbsp;</p>"
    echo "    <p>Best,</p>"
    echo "    <p>## sherpa-login-watcher for kairntech ##</p>"
    echo "  </body>"
    echo "</html>"
  } >> ${MAILTPL}.ts

  if [ "${NEWLINES}" != '' ] ; then
    sendmail -t -o message-content-type=html -f ${MAILFROM} < ${MAILTPL}.ts
  else
    echo " - Only whitelisted users detected - no mail to send"
  fi
  rm -f ${MAILTPL}.ts
else
  echo " - No sherpa-platform logins detected -"
fi

IFS=$OLDIFS
# End
echo "***** $( date ) Ending *****"
echo ""
