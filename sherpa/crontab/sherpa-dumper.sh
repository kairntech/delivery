#!/bin/bash
#0 */6 * * * /opt/monitor-sherpa/sherpa-dumper.sh 1>>/opt/monitor-sherpa/.log/sherpa-dumper.out 2>>/opt/monitor-sherpa/.log/sherpa-dumper.err

ROOT=/opt/monitor-sherpa
DUMPS=${ROOT}/.mongodumps
SHERPA=/opt/sherpa
HISTORY_KEEP=false
HISTORY_LIMIT=13 #3days, so 12 local backups

OLDIFS=$IFS
IFS=$'\n'
HISTORY_AWS_LIMIT=61 #15days, so 60 remote backups
HISTORY_AWS_DAYS=31  #30 days of old backups (filtered _000)
HISTORY_AWS_KEEP=false
LISTDBFILE=listDatabases.txt

# Start
echo "***** $( date ) Starting *****"

# Identify if MongoDB replSet or single
NBMONGO=$( docker ps | grep -c sherpa-mongodb )
if [ "${NBMONGO}" -ge 3 ] ; then
  MONGOHOST=sherpa-mongodb-primary
  MONGODUMP=/tmp/mongodump.gz
else
  MONGOHOST=sherpa-mongodb
  MONGODUMP=mongodump.gz
fi

# Create mongodump 
DATENOW=$( date +%Y%m%d_%H%M%S )
echo "   ** $( date ) checking if ${MONGOHOST} access is authenticated **"
MONGO_AUTH=false
MONGODB_USERNAME=
MONGODB_PASSWORD=
if [ -f "${SHERPA}"/.env ] ; then
  MONGODB_USERNAME=$( grep 'MONGODB_USERNAME' "${SHERPA}"/.env|cut -d '=' -f2 )
  MONGODB_USERNAME=${MONGODB_USERNAME:1:-1}
  MONGODB_PASSWORD=$( grep 'MONGODB_PASSWORD' "${SHERPA}"/.env|cut -d '=' -f2 )
  MONGODB_PASSWORD=${MONGODB_PASSWORD:1:-1}
fi
if [ ! -z "${MONGODB_USERNAME}" ] && [ ! -z "${MONGODB_PASSWORD}" ] ; then
  MONGO_AUTH=true
  echo "   ** $( date ) ${MONGOHOST} access is authenticated **"
else
  echo "   ** $( date ) ${MONGOHOST} access is not authenticated **"
fi
echo "   ** $( date ) creating dump ${MONGOHOST}:${MONGODUMP} **" 
docker exec -i "${MONGOHOST}" rm -f "${MONGODUMP}"
if [ "${MONGO_AUTH}" == "true" ] ; then
  docker exec -i "${MONGOHOST}" mongodump -u"${MONGODB_USERNAME}" -p"${MONGODB_PASSWORD}" --gzip --quiet --archive="${MONGODUMP}"
else
  docker exec -i "${MONGOHOST}" mongodump --gzip --quiet --archive="${MONGODUMP}"
fi
echo "   ** $( date ) retrieving dump in ${DUMPS}/mongodump-$( hostname )-${DATENOW}.gz **" 
docker cp "${MONGOHOST}":"${MONGODUMP}" "${DUMPS}"/mongodump-"$( hostname )"-"${DATENOW}".gz
docker exec -i "${MONGOHOST}" rm -f "${MONGODUMP}"
echo "   ** $( date ) creating listing of mongo databases ${LISTDBFILE} **" 
if [ "${MONGO_AUTH}" == "true" ] ; then
  docker exec -i "${MONGOHOST}" mongo -u"${MONGODB_USERNAME}" -p"${MONGODB_PASSWORD}" --quiet --eval 'db.adminCommand("listDatabases").databases.sort(function(l, r) {return r.sizeOnDisk - l.sizeOnDisk}).forEach(function(d) {print(d.name + " - " + (d.sizeOnDisk/1024/1024) + "M")});' > "${LISTDBFILE}"
else
  docker exec -i "${MONGOHOST}" mongo --quiet --eval 'db.adminCommand("listDatabases").databases.sort(function(l, r) {return r.sizeOnDisk - l.sizeOnDisk}).forEach(function(d) {print(d.name + " - " + (d.sizeOnDisk/1024/1024) + "M")});' > "${LISTDBFILE}"
fi
echo "   ** $( date ) getting backup of images folder **" 
rm -rf images && docker cp sherpa-core:/app/kairntech/sherpa/images images
tar cz -f images-"$( hostname )"-"${DATENOW}".tgz images && rm -rf images

# Publish on S3
echo "   ** $( date ) sending dump folder (${LISTDBFILE} + mongodump-$( hostname )-${DATENOW}.gz) to s3://kairntech/backups/$( hostname ) **" 
mkdir "${DUMPS}"/mongodump-"$( hostname )"-"${DATENOW}" && mv "${LISTDBFILE}" "${DUMPS}"/mongodump-"$( hostname )"-"${DATENOW}"/.
mv images-"$( hostname )"-"${DATENOW}".tgz "${DUMPS}"/mongodump-"$( hostname )"-"${DATENOW}"/.
mv "${DUMPS}"/mongodump-"$( hostname )"-"${DATENOW}".gz "${DUMPS}"/mongodump-"$( hostname )"-"${DATENOW}"/.
aws s3 cp --recursive --quiet "${DUMPS}"/mongodump-"$( hostname )"-"${DATENOW}" s3://kairntech/backups/"$( hostname )"/mongodump-"$( hostname )"-"${DATENOW}"

# Keep locally only last 10 dumps
if [ "${HISTORY_KEEP}" != "true" ] ; then
  FILES=$( find ${DUMPS} -maxdepth 1 -type d -name "*mongodump-*" -printf "%f\n"|sort -r|tail -n +${HISTORY_LIMIT} )
  #FILES=$( ls -tp ${DUMPS}|grep "mongodump-"|tail -n +${HISTORY_LIMIT} )
  for file in ${FILES} ; do
    echo "   ** $( date ) removing old dump folder ${DUMPS}/${file} **" 
    rm -rf "${DUMPS:?}"/"${file}"
  done
fi

DATEAWSNOW=$( date +%Y%m%d )
# Keep on aws only last 60 dumps
OLD_BACKUPS_NB=0
if [ "${HISTORY_AWS_KEEP}" != "true" ] ; then
  FILES=$( aws s3 ls s3://kairntech/backups/"$( hostname )"/|grep "mongodump-"|sort -rn|tail -n +${HISTORY_AWS_LIMIT} )
  for largefile in ${FILES} ; do
    file=$( echo "$largefile"|awk -F 'PRE' '{print $2}'|sed "s/ //g"|sed "s/\///g" )
    #Preserve backups generated at midnight
    if [[ $file != *${DATEAWSNOW}* ]] && [[ $file != *_000* ]] ; then
      echo "   ** $( date ) removing old dump folder s3://kairntech/backups/$( hostname )/${file} **" 
      aws s3 rm s3://kairntech/backups/"$( hostname )"/"$file" --recursive
    fi
    #Count old backups: delete older than limit
    if [[ $file == *_000* ]] ; then
      OLD_BACKUPS_NB=$(( OLD_BACKUPS_NB + 1 ))
    fi
    if [ $OLD_BACKUPS_NB -gt $HISTORY_AWS_DAYS ] ; then
      echo "   ** $( date ) removing very old dump folder s3://kairntech/backups/$( hostname )/${file} **" 
      aws s3 rm s3://kairntech/backups/"$( hostname )"/"$file" --recursive
    fi
  done
fi
IFS=$OLDIFS

# End
echo "***** $( date ) Ending *****"
echo ""
