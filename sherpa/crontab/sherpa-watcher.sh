#!/bin/bash
#*/5 * * * * /opt/monitor-sherpa/sherpa-watcher.sh 1>>/opt/monitor-sherpa/.log/sherpa-watcher.out 2>>/opt/monitor-sherpa/.log/sherpa-watcher.err

ROOT=/opt/monitor-sherpa
DROPSFILE=${ROOT}/.nbdrops

MAILTPL=${ROOT}/mailtosend.html
MAILFROM="info@kairntech.com"
PLATFORM="KAIRNTECH instance $( hostname ).kairntech.com"

# Start
echo "***** $( date ) Starting *****"

# Get number of dropDatabase
if [ ! -f "${DROPSFILE}" ] ; then
  echo "0" > "${DROPSFILE}"
fi
NB_MONGODROPSH=$( cat ${DROPSFILE} )
NB_MONGODROPS=$( docker exec sherpa-mongodb cat /var/log/mongodb/mongod.log 2>/dev/null | grep -c dropDatabase )

# Finalizing
if [ "${NB_MONGODROPS}" -ne "${NB_MONGODROPSH}" ] ; then
  NB_MONGODROPDIFF=$(( NB_MONGODROPS - NB_MONGODROPSH ))
  if [ ${NB_MONGODROPDIFF} -gt 0 ] ; then
    echo "${NB_MONGODROPS}" > ${DROPSFILE}
    DATENOW=$( date )
    echo " - Looks like I have to send an email"
    cp ${MAILTPL} ${MAILTPL}.ts
    sed -i "s|PLATFORM|$PLATFORM|g" ${MAILTPL}.ts
    sed -i "s|MAILFROM|$MAILFROM|g" ${MAILTPL}.ts
    sed -i "s|DROPDATABASE|$NB_MONGODROPDIFF|g" ${MAILTPL}.ts
    sed -i "s|DATE|$DATENOW|g" ${MAILTPL}.ts
    sendmail -vt -o message-content-type=html -f ${MAILFROM} < ${MAILTPL}.ts
    rm -f ${MAILTPL}.ts
  fi
else
  echo " - No sherpa [dropDatabase] detected -"
fi

# End
echo "***** $( date ) Ending *****"
echo ""
