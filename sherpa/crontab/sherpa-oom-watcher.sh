#!/bin/bash
#*/5 * * * * /opt/monitor-sherpa/sherpa-oom-watcher.sh 1>>/opt/monitor-sherpa/.log/sherpa-oom-watcher.out 2>>/opt/monitor-sherpa/.log/sherpa-oom-watcher.err

ROOT=/opt/monitor-sherpa
FILE_OOMSHERPA=${ROOT}/.nbooms-sherpa
FILE_OOMCRF=${ROOT}/.nbooms-crf
FILE_OOMEF=${ROOT}/.nbooms-ef
FILE_OOMFASTTEXT=${ROOT}/.nbooms-fasttext
FILE_OOMPM=${ROOT}/.nbooms-pm
FILE_OOMDELFT=${ROOT}/.nbooms-delft
FILE_OOMSPACY=${ROOT}/.nbooms-spacy
FILE_OOMFLAIR=${ROOT}/.nbooms-flair
FILE_OOMSKLEARN=${ROOT}/.nbooms-sklearn
FILE_OOMBERTOPIC=${ROOT}/.nbooms-bertopic
FILE_OOMTRANKIT=${ROOT}/.nbooms-trankit
FILE_OOMTRF=${ROOT}/.nbooms-trf

MAILTPL=${ROOT}/mailtosend.oom.html
MAILFROM="info@kairntech.com"
PLATFORM="OVH instance $( hostname ).kairntech.com"

# Start
echo "***** $( date ) Starting *****"

# Get number of OutOfMemory
STRING_TO_MATCH='segmentation fault\| oom\|OutOfMemoryError\|Java heap space'
STRING_TO_MATCH_SHERPA='SIGSEGV\|OutOfMemoryError\|Java heap space'

# Control on sherpa-core
NB_OOMSHERPA=0
NB_HPROFSHERPA=0
if [ ! -f "${FILE_OOMSHERPA}" ] ; then
  echo "0" > "${FILE_OOMSHERPA}"
fi
NB_OOMSHERPAH=$( cat "${FILE_OOMSHERPA}" )
PS_OOMSHERPA=$( docker ps|grep -c 'sherpa-core' )
if [ "${PS_OOMSHERPA}" -eq 1 ] ; then
  NB_OOMSHERPA1=$( docker exec sherpa-core cat /var/log/sherpa/sherpa.log 2>/dev/null | grep -c "$STRING_TO_MATCH_SHERPA" )
  NB_OOMSHERPA2=$( docker logs sherpa-core 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMSHERPA=$(( NB_OOMSHERPA1 + NB_OOMSHERPA2 ))
  NB_HPROFSHERPA=$( docker exec sherpa-core find /app/kairntech/sherpa/data|grep -c "hprof" )
fi

# Control on sherpa-crfsuite-suggester
NB_OOMCRF=0
if [ ! -f "${FILE_OOMCRF}" ] ; then
  echo "0" > "${FILE_OOMCRF}"
fi
NB_OOMCRFH=$( cat "${FILE_OOMCRF}" )
PS_OOMCRF=$( docker ps|grep -c 'sherpa-crfsuite-suggester' )
if [ "${PS_OOMCRF}" -eq 1 ] ; then
  NB_OOMCRF1=$( docker exec sherpa-crfsuite-suggester cat /var/log/crfsuite-suggester/pysuggester.json.log.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMCRF2=$( docker logs sherpa-crfsuite-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMCRF=$(( NB_OOMCRF1 + NB_OOMCRF2 ))
fi

# Control on sherpa-entityfishing-suggester
NB_OOMEF=0
if [ ! -f "${FILE_OOMEF}" ] ; then
  echo "0" > "${FILE_OOMEF}"
fi
NB_OOMEFH=$( cat "${FILE_OOMEF}" )
PS_OOMEF=$( docker ps|grep -c 'sherpa-entityfishing-suggester' )
if [ "${PS_OOMEF}" -eq 1 ] ; then
  NB_OOMEF1=$( docker exec sherpa-entityfishing-suggester cat /var/log/entityfishing-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMEF2=$( docker logs sherpa-entityfishing-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMEF=$(( NB_OOMEF1 + NB_OOMEF2 ))
fi

# Control on sherpa-fasttext-suggester
NB_OOMFASTTEXT=0
if [ ! -f "${FILE_OOMFASTTEXT}" ] ; then
  echo "0" > "${FILE_OOMFASTTEXT}"
fi
NB_OOMFASTTEXTH=$( cat "${FILE_OOMFASTTEXT}" )
PS_OOMFASTTEXT=$( docker ps|grep -c 'sherpa-fasttext-suggester' )
if [ "${PS_OOMFASTTEXT}" -eq 1 ] ; then
  NB_OOMFASTTEXT1=$( docker exec sherpa-fasttext-suggester cat /var/log/fasttext-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMFASTTEXT2=$( docker logs sherpa-fasttext-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMFASTTEXT=$(( NB_OOMFASTTEXT1 + NB_OOMFASTTEXT2 ))
fi

# Control on sherpa-phrasematcher-xxx-suggester
NB_OOMPM=0
if [ ! -f "${FILE_OOMPM}" ] ; then
  echo "0" > "${FILE_OOMPM}"
fi
NB_OOMPMH=$( cat "${FILE_OOMPM}" )
PS_OOMPM=$( docker ps|grep -c 'sherpa-phrasematcher-test-suggester\|sherpa-phrasematcher-train-suggester' )
if [ "${PS_OOMPM}" -eq 2 ] ; then
  NB_OOMPM_TEST1=$( docker exec sherpa-phrasematcher-test-suggester cat /var/log/phrasematcher-test-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMPM_TEST2=$( docker logs sherpa-phrasematcher-test-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMPM_TEST=$(( NB_OOMPM_TEST1 + NB_OOMPM_TEST2 ))
  NB_OOMPM_TRAIN1=$( docker exec sherpa-phrasematcher-train-suggester cat /var/log/phrasematcher-train-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMPM_TRAIN2=$( docker logs sherpa-phrasematcher-train-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMPM_TRAIN=$(( NB_OOMPM_TRAIN1 + NB_OOMPM_TRAIN2 ))
  NB_OOMPM=$(( NB_OOMPM_TEST + NB_OOMPM_TRAIN ))
fi

# Control on sherpa-delft-xxx-suggester
NB_OOMDELFT=0
if [ ! -f "${FILE_OOMDELFT}" ] ; then
  echo "0" > "${FILE_OOMDELFT}"
fi
NB_OOMDELFTH=$( cat "${FILE_OOMDELFT}" )
PS_OOMDELFT=$( docker ps|grep -c 'sherpa-delft-test-suggester\|sherpa-delft-train-suggester' )
if [ "${PS_OOMDELFT}" -eq 2 ] ; then
  NB_OOMDELFT_TEST1=$( docker exec sherpa-delft-test-suggester cat /var/log/delft-test-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMDELFT_TEST2=$( docker logs sherpa-delft-test-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMDELFT_TEST=$(( NB_OOMDELFT_TEST1 + NB_OOMDELFT_TEST2 ))
  NB_OOMDELFT_TRAIN1=$( docker exec sherpa-delft-train-suggester cat /var/log/delft-train-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMDELFT_TRAIN2=$( docker logs sherpa-delft-train-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMDELFT_TRAIN=$(( NB_OOMDELFT_TRAIN1 + NB_OOMDELFT_TRAIN2 ))
  NB_OOMDELFT=$(( NB_OOMDELFT_TEST + NB_OOMDELFT_TRAIN ))
fi

# Control on sherpa-spacy-xxx-suggester
NB_OOMSPACY=0
if [ ! -f "${FILE_OOMSPACY}" ] ; then
  echo "0" > "${FILE_OOMSPACY}"
fi
NB_OOMSPACYH=$( cat "${FILE_OOMSPACY}" )
PS_OOMSPACY=$( docker ps|grep -c 'sherpa-spacy-test-suggester\|sherpa-spacy-train-suggester' )
if [ "${PS_OOMSPACY}" -eq 2 ] ; then
  NB_OOMSPACY_TEST1=$( docker exec sherpa-spacy-test-suggester cat /var/log/spacy-test-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMSPACY_TEST2=$( docker logs sherpa-spacy-test-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMSPACY_TEST=$(( NB_OOMSPACY_TEST1 + NB_OOMSPACY_TEST2 ))
  NB_OOMSPACY_TRAIN1=$( docker exec sherpa-spacy-train-suggester cat /var/log/spacy-train-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMSPACY_TRAIN2=$( docker logs sherpa-spacy-train-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMSPACY_TRAIN=$(( NB_OOMSPACY_TRAIN1 + NB_OOMSPACY_TRAIN2 ))
  NB_OOMSPACY=$(( NB_OOMSPACY_TEST + NB_OOMSPACY_TRAIN ))
fi

# Control on sherpa-flair-xxx-suggester
NB_OOMFLAIR=0
if [ ! -f "${FILE_OOMFLAIR}" ] ; then
  echo "0" > "${FILE_OOMFLAIR}"
fi
NB_OOMFLAIRH=$( cat "${FILE_OOMFLAIR}" )
PS_OOMFLAIR=$( docker ps|grep -c 'sherpa-flair-test-suggester\|sherpa-flair-train-suggester' )
if [ "${PS_OOMFLAIR}" -eq 2 ] ; then
  NB_OOMFLAIR_TEST1=$( docker exec sherpa-flair-test-suggester cat /var/log/flair-test-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMFLAIR_TEST2=$( docker logs sherpa-flair-test-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMFLAIR_TEST=$(( NB_OOMFLAIR_TEST1 + NB_OOMFLAIR_TEST2 ))
  NB_OOMFLAIR_TRAIN1=$( docker exec sherpa-flair-train-suggester cat /var/log/flair-train-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMFLAIR_TRAIN2=$( docker logs sherpa-flair-train-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMFLAIR_TRAIN=$(( NB_OOMFLAIR_TRAIN1 + NB_OOMFLAIR_TRAIN2 ))
  NB_OOMFLAIR=$(( NB_OOMFLAIR_TEST + NB_OOMFLAIR_TRAIN ))
fi

# Control on sherpa-sklearn-xxx-suggester
NB_OOMSKLEARN=0
if [ ! -f "${FILE_OOMSKLEARN}" ] ; then
  echo "0" > "${FILE_OOMSKLEARN}"
fi
NB_OOMSKLEARNH=$( cat "${FILE_OOMSKLEARN}" )
PS_OOMSKLEARN=$( docker ps|grep -c 'sherpa-sklearn-test-suggester\|sherpa-sklearn-train-suggester' )
if [ "${PS_OOMSKLEARN}" -eq 2 ] ; then
  NB_OOMSKLEARN_TEST1=$( docker exec sherpa-sklearn-test-suggester cat /var/log/sklearn-test-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMSKLEARN_TEST2=$( docker logs sherpa-sklearn-test-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMSKLEARN_TEST=$(( NB_OOMSKLEARN_TEST1 + NB_OOMSKLEARN_TEST2 ))
  NB_OOMSKLEARN_TRAIN1=$( docker exec sherpa-sklearn-train-suggester cat /var/log/sklearn-train-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMSKLEARN_TRAIN2=$( docker logs sherpa-sklearn-train-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMSKLEARN_TRAIN=$(( NB_OOMSKLEARN_TRAIN1 + NB_OOMSKLEARN_TRAIN2 ))
  NB_OOMSKLEARN=$(( NB_OOMSKLEARN_TEST + NB_OOMSKLEARN_TRAIN ))
fi

# Control on sherpa-bertopic-xxx-suggester
NB_OOMBERTOPIC=0
if [ ! -f "${FILE_OOMBERTOPIC}" ] ; then
  echo "0" > "${FILE_OOMBERTOPIC}"
fi
NB_OOMBERTOPICH=$( cat "${FILE_OOMBERTOPIC}" )
PS_OOMBERTOPIC=$( docker ps|grep -c 'sherpa-bertopic-test-suggester\|sherpa-bertopic-train-suggester' )
if [ "${PS_OOMBERTOPIC}" -eq 2 ] ; then
  NB_OOMBERTOPIC_TEST1=$( docker exec sherpa-bertopic-test-suggester cat /var/log/bertopic-test-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMBERTOPIC_TEST2=$( docker logs sherpa-bertopic-test-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMBERTOPIC_TEST=$(( NB_OOMBERTOPIC_TEST1 + NB_OOMBERTOPIC_TEST2 ))
  NB_OOMBERTOPIC_TRAIN1=$( docker exec sherpa-bertopic-train-suggester cat /var/log/bertopic-train-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMBERTOPIC_TRAIN2=$( docker logs sherpa-bertopic-train-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMBERTOPIC_TRAIN=$(( NB_OOMBERTOPIC_TRAIN1 + NB_OOMBERTOPIC_TRAIN2 ))
  NB_OOMBERTOPIC=$(( NB_OOMBERTOPIC_TEST + NB_OOMBERTOPIC_TRAIN ))
fi

# Control on sherpa-trankit-xxx-suggester
NB_OOMTRANKIT=0
if [ ! -f "${FILE_OOMTRANKIT}" ] ; then
  echo "0" > "${FILE_OOMTRANKIT}"
fi
NB_OOMTRANKITH=$( cat "${FILE_OOMTRANKIT}" )
PS_OOMTRANKIT=$( docker ps|grep -c 'sherpa-trankit-test-suggester\|sherpa-trankit-train-suggester' )
if [ "${PS_OOMTRANKIT}" -eq 2 ] ; then
  NB_OOMTRANKIT_TEST1=$( docker exec sherpa-trankit-test-suggester cat /var/log/trankit-test-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMTRANKIT_TEST2=$( docker logs sherpa-trankit-test-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMTRANKIT_TEST=$(( NB_OOMTRANKIT_TEST1 + NB_OOMTRANKIT_TEST2 ))
  NB_OOMTRANKIT_TRAIN1=$( docker exec sherpa-trankit-train-suggester cat /var/log/trankit-train-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMTRANKIT_TRAIN2=$( docker logs sherpa-trankit-train-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMTRANKIT_TRAIN=$(( NB_OOMTRANKIT_TRAIN1 + NB_OOMTRANKIT_TRAIN2 ))
  NB_OOMTRANKIT=$(( NB_OOMTRANKIT_TEST + NB_OOMTRANKIT_TRAIN ))
fi

# Control on sherpa-trf-xxx-suggester
NB_OOMTRF=0
if [ ! -f "${FILE_OOMTRF}" ] ; then
  echo "0" > "${FILE_OOMTRF}"
fi
NB_OOMTRFH=$( cat "${FILE_OOMTRF}" )
PS_OOMTRF=$( docker ps|grep -c 'sherpa-trf-test-suggester\|sherpa-trf-train-suggester' )
if [ "${PS_OOMTRF}" -eq 2 ] ; then
  NB_OOMTRF_TEST1=$( docker exec sherpa-trf-test-suggester cat /var/log/trf-test-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMTRF_TEST2=$( docker logs sherpa-trf-test-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMTRF_TEST=$(( NB_OOMTRF_TEST1 + NB_OOMTRF_TEST2 ))
  NB_OOMTRF_TRAIN1=$( docker exec sherpa-trf-train-suggester cat /var/log/trf-train-suggester/pysuggester.json.log 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMTRF_TRAIN2=$( docker logs sherpa-trf-train-suggester 2>/dev/null | grep -c "$STRING_TO_MATCH" )
  NB_OOMTRF_TRAIN=$(( NB_OOMTRF_TRAIN1 + NB_OOMTRF_TRAIN2 ))
  NB_OOMTRF=$(( NB_OOMTRF_TEST + NB_OOMTRF_TRAIN ))
fi

# Finalizing
OOM_SHERPA=false
NB_OOMSHERPADIFF=0
if [ "${NB_OOMSHERPA}" -ne "${NB_OOMSHERPAH}" ] ; then
  if [ ${NB_OOMSHERPA} -ne 0 ] ; then
    NB_OOMSHERPADIFF=$(( NB_OOMSHERPA - NB_OOMSHERPAH ))
    if [ ${NB_OOMSHERPADIFF} -gt 0 ] ; then
      echo "${NB_OOMSHERPA}" > ${FILE_OOMSHERPA}
      echo " - Looks like I have to send an email: oom in sherpa-core"
      OOM_SHERPA=true
    fi
  fi
fi
OOM_CRF=false
NB_OOMCRFDIFF=0
if [ "${NB_OOMCRF}" -ne "${NB_OOMCRFH}" ] ; then
  if [ ${NB_OOMCRF} -ne 0 ] ; then
    NB_OOMCRFDIFF=$(( NB_OOMCRF - NB_OOMCRFH ))
    if [ ${NB_OOMCRFDIFF} -gt 0 ] ; then
      echo "${NB_OOMCRF}" > ${FILE_OOMCRF}
      echo " - Looks like I have to send an email: oom in sherpa-crfsuite-suggester"
      OOM_CRF=true
    fi
  fi
fi
OOM_EF=false
NB_OOMEFDIFF=0
if [ "${NB_OOMEF}" -ne "${NB_OOMEFH}" ] ; then
  if [ ${NB_OOMEF} -ne 0 ] ; then
    NB_OOMEFDIFF=$(( NB_OOMEF - NB_OOMEFH ))
    if [ ${NB_OOMEFDIFF} -gt 0 ] ; then
      echo "${NB_OOMEF}" > ${FILE_OOMEF}
      echo " - Looks like I have to send an email: oom in sherpa-entityfishing-suggester"
      OOM_EF=true
    fi
  fi
fi
OOM_FASTTEXT=false
NB_OOMFASTTEXTDIFF=0
if [ "${NB_OOMFASTTEXT}" -ne "${NB_OOMFASTTEXTH}" ] ; then
  if [ ${NB_OOMFASTTEXT} -ne 0 ] ; then
    NB_OOMFASTTEXTDIFF=$(( NB_OOMFASTTEXT - NB_OOMFASTTEXTH ))
    if [ ${NB_OOMFASTTEXTDIFF} -gt 0 ] ; then
      echo "${NB_OOMFASTTEXT}" > ${FILE_OOMFASTTEXT}
      echo " - Looks like I have to send an email: oom in sherpa-fasttext-suggester"
      OOM_FASTTEXT=true
    fi
  fi
fi
OOM_PM=false
NB_OOMPMDIFF=0
if [ "${NB_OOMPM}" -ne "${NB_OOMPMH}" ] ; then
  if [ ${NB_OOMPM} -ne 0 ] ; then
    NB_OOMPMDIFF=$(( NB_OOMPM - NB_OOMPMH ))
    if [ ${NB_OOMPMDIFF} -gt 0 ] ; then
      echo "${NB_OOMPM}" > ${FILE_OOMPM}
      echo " - Looks like I have to send an email: oom in sherpa-phrasematcher-xxx-suggester"
      OOM_PM=true
    fi
  fi
fi
OOM_DELFT=false
NB_OOMDELFTDIFF=0
if [ "${NB_OOMDELFT}" -ne "${NB_OOMDELFTH}" ] ; then
  if [ ${NB_OOMDELFT} -ne 0 ] ; then
    NB_OOMDELFTDIFF=$(( NB_OOMDELFT - NB_OOMDELFTH ))
    if [ ${NB_OOMDELFTDIFF} -gt 0 ] ; then
      echo "${NB_OOMDELFT}" > ${FILE_OOMDELFT}
      echo " - Looks like I have to send an email: oom in sherpa-delft-xxx-suggester"
      OOM_DELFT=true
    fi
  fi
fi
OOM_SPACY=false
NB_OOMSPACYDIFF=0
if [ "${NB_OOMSPACY}" -ne "${NB_OOMSPACYH}" ] ; then
  if [ ${NB_OOMSPACY} -ne 0 ] ; then
    NB_OOMSPACYDIFF=$(( NB_OOMSPACY - NB_OOMSPACYH ))
    if [ ${NB_OOMSPACYDIFF} -gt 0 ] ; then
      echo "${NB_OOMSPACY}" > ${FILE_OOMSPACY}
      echo " - Looks like I have to send an email: oom in sherpa-spacy-xxx-suggester"
      OOM_SPACY=true
    fi
  fi
fi
OOM_FLAIR=false
NB_OOMFLAIRDIFF=0
if [ "${NB_OOMFLAIR}" -ne "${NB_OOMFLAIRH}" ] ; then
  if [ ${NB_OOMFLAIR} -ne 0 ] ; then
    NB_OOMFLAIRDIFF=$(( NB_OOMFLAIR - NB_OOMFLAIRH ))
    if [ ${NB_OOMFLAIRDIFF} -gt 0 ] ; then
      echo "${NB_OOMFLAIR}" > ${FILE_OOMFLAIR}
      echo " - Looks like I have to send an email: oom in sherpa-flair-xxx-suggester"
      OOM_FLAIR=true
    fi
  fi
fi
OOM_SKLEARN=false
NB_OOMSKLEARNDIFF=0
if [ "${NB_OOMSKLEARN}" -ne "${NB_OOMSKLEARNH}" ] ; then
  if [ ${NB_OOMSKLEARN} -ne 0 ] ; then
    NB_OOMSKLEARNDIFF=$(( NB_OOMSKLEARN - NB_OOMSKLEARNH ))
    if [ ${NB_OOMSKLEARNDIFF} -gt 0 ] ; then
      echo "${NB_OOMSKLEARN}" > ${FILE_OOMSKLEARN}
      echo " - Looks like I have to send an email: oom in sherpa-sklearn-xxx-suggester"
      OOM_SKLEARN=true
    fi
  fi
fi
OOM_BERTOPIC=false
NB_OOMBERTOPICDIFF=0
if [ "${NB_OOMBERTOPIC}" -ne "${NB_OOMBERTOPICH}" ] ; then
  if [ ${NB_OOMBERTOPIC} -ne 0 ] ; then
    NB_OOMBERTOPICDIFF=$(( NB_OOMBERTOPIC - NB_OOMBERTOPICH ))
    if [ ${NB_OOMBERTOPICDIFF} -gt 0 ] ; then
      echo "${NB_OOMBERTOPIC}" > ${FILE_OOMBERTOPIC}
      echo " - Looks like I have to send an email: oom in sherpa-bertopic-xxx-suggester"
      OOM_BERTOPIC=true
    fi
  fi
fi
OOM_TRANKIT=false
NB_OOMTRANKITDIFF=0
if [ "${NB_OOMTRANKIT}" -ne "${NB_OOMTRANKITH}" ] ; then
  if [ ${NB_OOMTRANKIT} -ne 0 ] ; then
    NB_OOMTRANKITDIFF=$(( NB_OOMTRANKIT - NB_OOMTRANKITH ))
    if [ ${NB_OOMTRANKITDIFF} -gt 0 ] ; then
      echo "${NB_OOMTRANKIT}" > ${FILE_OOMTRANKIT}
      echo " - Looks like I have to send an email: oom in sherpa-trankit-xxx-suggester"
      OOM_TRANKIT=true
    fi
  fi
fi
OOM_TRF=false
NB_OOMTRFDIFF=0
if [ "${NB_OOMTRF}" -ne "${NB_OOMTRFH}" ] ; then
  if [ ${NB_OOMTRF} -ne 0 ] ; then
    NB_OOMTRFDIFF=$(( NB_OOMTRF - NB_OOMTRFH ))
    if [ ${NB_OOMTRFDIFF} -gt 0 ] ; then
      echo "${NB_OOMTRF}" > ${FILE_OOMTRF}
      echo " - Looks like I have to send an email: oom in sherpa-trf-xxx-suggester"
      OOM_TRF=true
    fi
  fi
fi

ALERTCOLOR='red'
if [ "$OOM_SHERPA" == "true" ]  || [ "$OOM_CRF" == "true" ]      || \
   [ "$OOM_EF" == "true" ]      || [ "$OOM_FASTTEXT" == "true" ] || \
   [ "$OOM_PM" == "true" ]      || [ "$OOM_DELFT" == "true" ]    || \
   [ "$OOM_SPACY" == "true" ]   || [ "$OOM_FLAIR" == "true" ]    || \
   [ "$OOM_SKLEARN" == "true" ] || [ "$OOM_BERTOPIC" == "true" ] || \
   [ "$OOM_TRANKIT" == "true" ] || [ "$OOM_TRF" == "true" ]      || \
   [ "$NB_HPROFSHERPA" -ne 0 ] ; then
  DATENOW=$( date )
  cp ${MAILTPL} ${MAILTPL}.ts
  sed -i "s|PLATFORM|$PLATFORM|g" ${MAILTPL}.ts
  sed -i "s|MAILFROM|$MAILFROM|g" ${MAILTPL}.ts
  if [ "$OOM_SHERPA" == "true" ]  || [ "$OOM_CRF" == "true" ]      || \
     [ "$OOM_EF" == "true" ]      || [ "$OOM_FASTTEXT" == "true" ] || \
     [ "$OOM_PM" == "true" ]      || [ "$OOM_DELFT" == "true" ]    || \
     [ "$OOM_SPACY" == "true" ]   || [ "$OOM_FLAIR" == "true" ]    || \
     [ "$OOM_SKLEARN" == "true" ] || [ "$OOM_BERTOPIC" == "true" ] || \
     [ "$OOM_TRANKIT" == "true" ] || [ "$OOM_TRF" == "true" ] ; then
    sed -i "s|OOMDISPLAY|inherit|g" ${MAILTPL}.ts
  else
    sed -i "s|OOMDISPLAY|none|g" ${MAILTPL}.ts
  fi
  if [ "$NB_HPROFSHERPA" -ne 0 ] ; then
    echo " - Looks like I have to send an email: hprof file found in sherpa-core"
    sed -i "s|HPROFDISPLAY|inherit|g" ${MAILTPL}.ts
  else
    sed -i "s|HPROFDISPLAY|none|g" ${MAILTPL}.ts
  fi
  if [ "$OOM_SHERPA" == "true" ] ; then
    sed -i "s|SHERPACOLOR|$ALERTCOLOR|g" ${MAILTPL}.ts
    sed -i "s|SHERPADISPLAY|list-item|g" ${MAILTPL}.ts
  else
    sed -i "s|SHERPADISPLAY|none|g" ${MAILTPL}.ts
  fi
  if [ "$OOM_CRF" == "true" ] ; then
    sed -i "s|CRFCOLOR|$ALERTCOLOR|g" ${MAILTPL}.ts
    sed -i "s|CRFDISPLAY|list-item|g" ${MAILTPL}.ts
  else
    sed -i "s|CRFDISPLAY|none|g" ${MAILTPL}.ts
  fi
  if [ "$OOM_EF" == "true" ] ; then
    sed -i "s|EFCOLOR|$ALERTCOLOR|g" ${MAILTPL}.ts
    sed -i "s|EFDISPLAY|list-item|g" ${MAILTPL}.ts
  else
    sed -i "s|EFDISPLAY|none|g" ${MAILTPL}.ts
  fi
  if [ "$OOM_FASTTEXT" == "true" ] ; then
    sed -i "s|FASTTEXTCOLOR|$ALERTCOLOR|g" ${MAILTPL}.ts
    sed -i "s|FASTTEXTDISPLAY|list-item|g" ${MAILTPL}.ts
  else
    sed -i "s|FASTTEXTDISPLAY|none|g" ${MAILTPL}.ts
  fi
  if [ "$OOM_PM" == "true" ] ; then
    sed -i "s|PMCOLOR|$ALERTCOLOR|g" ${MAILTPL}.ts
    sed -i "s|PMDISPLAY|list-item|g" ${MAILTPL}.ts
  else
    sed -i "s|PMDISPLAY|none|g" ${MAILTPL}.ts
  fi
  if [ "$OOM_DELFT" == "true" ] ; then
    sed -i "s|DELFTCOLOR|$ALERTCOLOR|g" ${MAILTPL}.ts
    sed -i "s|DELFTDISPLAY|list-item|g" ${MAILTPL}.ts
  else
    sed -i "s|DELFTDISPLAY|none|g" ${MAILTPL}.ts
  fi
  if [ "$OOM_SPACY" == "true" ] ; then
    sed -i "s|SPACYCOLOR|$ALERTCOLOR|g" ${MAILTPL}.ts
    sed -i "s|SPACYDISPLAY|list-item|g" ${MAILTPL}.ts
  else
    sed -i "s|SPACYDISPLAY|none|g" ${MAILTPL}.ts
  fi
  if [ "$OOM_FLAIR" == "true" ] ; then
    sed -i "s|FLAIRCOLOR|$ALERTCOLOR|g" ${MAILTPL}.ts
    sed -i "s|FLAIRDISPLAY|list-item|g" ${MAILTPL}.ts
  else
    sed -i "s|FLAIRDISPLAY|none|g" ${MAILTPL}.ts
  fi
  if [ "$OOM_SKLEARN" == "true" ] ; then
    sed -i "s|SKLEARNCOLOR|$ALERTCOLOR|g" ${MAILTPL}.ts
    sed -i "s|SKLEARNDISPLAY|list-item|g" ${MAILTPL}.ts
  else
    sed -i "s|SKLEARNDISPLAY|none|g" ${MAILTPL}.ts
  fi
  if [ "$OOM_BERTOPIC" == "true" ] ; then
    sed -i "s|BERTOPICCOLOR|$ALERTCOLOR|g" ${MAILTPL}.ts
    sed -i "s|BERTOPICDISPLAY|list-item|g" ${MAILTPL}.ts
  else
    sed -i "s|BERTOPICDISPLAY|none|g" ${MAILTPL}.ts
  fi
  if [ "$OOM_TRANKIT" == "true" ] ; then
    sed -i "s|TRANKITCOLOR|$ALERTCOLOR|g" ${MAILTPL}.ts
    sed -i "s|TRANKITDISPLAY|list-item|g" ${MAILTPL}.ts
  else
    sed -i "s|TRANKITDISPLAY|none|g" ${MAILTPL}.ts
  fi
  if [ "$OOM_TRF" == "true" ] ; then
    sed -i "s|TRFCOLOR|$ALERTCOLOR|g" ${MAILTPL}.ts
    sed -i "s|TRFDISPLAY|list-item|g" ${MAILTPL}.ts
  else
    sed -i "s|TRFDISPLAY|none|g" ${MAILTPL}.ts
  fi
  sed -i "s|DATE|$DATENOW|g" ${MAILTPL}.ts
  sendmail -t -o message-content-type=html -f ${MAILFROM} < ${MAILTPL}.ts
  rm -f ${MAILTPL}.ts
else
  echo " - No sherpa-platform [oom/hprof] detected -"
fi

# End
echo "***** $( date ) Ending *****"
echo ""
