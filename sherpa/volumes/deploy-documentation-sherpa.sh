#!/bin/bash

################################################################
PATH_DOC=/doc
################################################################

################################################################
# Debian 9/stretch moved to archive.debian.org
# https://lists.debian.org/debian-devel-announce/2023/03/msg00006.html
sed -i s/deb.debian.org/archive.debian.org/g /etc/apt/sources.list
sed -i 's|security.debian.org|archive.debian.org/|g' /etc/apt/sources.list
sed -i '/stretch-updates/d' /etc/apt/sources.list
################################################################

################################################################
# Install prerequisites (apt-get)
WGET=$( which wget )
if [ -z "$WGET" ]; then
  apt-get update
  apt install -y wget
fi

TAR=$( which tar )
if [ -z "$TAR" ]; then
  apt-get update
  apt install -y tar
fi
################################################################

################################################################
# Catch environment
if [ -z "$1" ] ; then
  echo " - You need to add parameters such as $0 2022.12.15 to deploy Documentation v.2022.12.15 prerequisites, exiting.."
  exit 0
elif [ "$1" == "h" ] || [ "$1" == "-h" ] || [ "$1" == "help" ] || [ "$1" == "--help" ] ; then
  echo " - Usage of $0 is:"
  echo " ---  $0 2022.12.15                     --- to deploy Documentation v.2022.12.15 prerequisites"
  exit 0
fi

# Manage parameters
BASE_URL="http://51.38.230.236:8081/artifactory/libs-release-local/com/kairntech/doc/"
DOC_VERSION=$1

################################################################
# Verbose output
GDATE_START=$( date +%s )
echo "[$(date)]:  Starting download of Documentation prerequisites"
echo "[$(date)]:  Downloading from URL ${BASE_URL}"
echo " - You asked to download Documentation v${DOC_VERSION} prerequisites"
################################################################

#-------- DOCUMENTATION --------
DATE_START=$( date +%s )
echo " --[$(date)]: Starting deployment of Documentation v${DOC_VERSION} prerequisites"
if [ ! -d "${PATH_DOC}" ] ; then
  mkdir -p "${PATH_DOC}"
fi
echo " --[$(date)]: |-Downloading Documentation v${DOC_VERSION} prerequisites"
cd "${PATH_DOC}"
wget -q "${BASE_URL}"/"${DOC_VERSION}"/doc-"${DOC_VERSION}".tgz
echo " --[$(date)]: |-Deploying Documentation v${DOC_VERSION} prerequisites [doc-${DOC_VERSION}.tgz]"
tar xz -f doc-"${DOC_VERSION}".tgz
rm -f doc-"${DOC_VERSION}".tgz
DATE_END=$( date +%s )
DIFF_SECONDS=$(( DATE_END - DATE_START ))
ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
echo " --[$(date)]: Ending deployment of Documentation v${DOC_VERSION} prerequisites [${ELAPSED}]"
#-------- DOCUMENTATION --------

# Add non-root kairntech and give permissions to workdir
USER_ID=500
GROUP_ID=500
groupadd --gid $GROUP_ID kairntech
adduser kairntech --ingroup kairntech --gecos '' --disabled-password --uid $USER_ID
chown -R kairntech:kairntech "${PATH_DOC}"

GDATE_END=$( date +%s )
DIFF_SECONDS=$(( GDATE_END - GDATE_START ))
GELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
echo "[$(date)]: Ending download of Documentation prerequisites [${GELAPSED}]"
