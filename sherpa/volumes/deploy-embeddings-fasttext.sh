#!/bin/bash
  
################################################################
DOWNLOAD_FASTTEXT_AR=false
DOWNLOAD_FASTTEXT_DE=false
DOWNLOAD_FASTTEXT_EN=false
DOWNLOAD_FASTTEXT_ES=false
DOWNLOAD_FASTTEXT_FR=false
DOWNLOAD_FASTTEXT_IT=false
DOWNLOAD_FASTTEXT_JA=false
DOWNLOAD_FASTTEXT_PT=false
DOWNLOAD_FASTTEXT_RU=false
DOWNLOAD_FASTTEXT_ZH=false
################################################################
PATH_EMBEDDINGS=/data/embeddings
################################################################

################################################################
# Wiki Fasttext
EMBEDDING_FASTTEXT_WIKI_AR_FILES="wiki.ar.vec"
EMBEDDING_FASTTEXT_WIKI_DE_FILES="wiki.de.vec"
EMBEDDING_FASTTEXT_WIKI_EN_FILES="wiki.en.vec"
EMBEDDING_FASTTEXT_WIKI_ES_FILES="wiki.es.vec"
EMBEDDING_FASTTEXT_WIKI_FR_FILES="wiki.fr.vec"
EMBEDDING_FASTTEXT_WIKI_IT_FILES="wiki.it.vec"
EMBEDDING_FASTTEXT_WIKI_JA_FILES="wiki.ja.vec"
EMBEDDING_FASTTEXT_WIKI_PT_FILES="wiki.pt.vec"
EMBEDDING_FASTTEXT_WIKI_RU_FILES="wiki.ru.vec"
EMBEDDING_FASTTEXT_WIKI_ZH_FILES="wiki.zh.vec"
################################################################

################################################################
# Catch environment
if [ -z "$1" ] ; then
  echo " - You need to add parameters such as $0 en to deploy FastText EN prerequisites, exiting.."
  exit 0
elif [ "$1" == "h" ] || [ "$1" == "-h" ] || [ "$1" == "help" ] || [ "$1" == "--help" ] ; then
  echo " - Usage of $0 is:"
  echo " ---  $0 en                       --- to deploy FastText EN prerequisites"
  echo " ---  $0 de,en                    --- to deploy FastText DE + EN prerequisites"
  echo " ---  $0 all                      --- to deploy all FastText prerequisites"
  echo " ---  $0 http://localhost all     --- to deploy all FastText prerequisites, from http://localhost"
  exit 0
fi

# Manage parameters
BASE_URL="https://kairntech.s3.eu-west-3.amazonaws.com"
if [[ $1 == *"http"* ]] ; then
  BASE_URL=$1
  if [ -z "$2" ] || [ "$2" == "" ] ; then
    _LANGS="all"
  else
    _LANGS=$2
  fi
else
  if [ -z "$1" ] || [ "$1" == "" ] ; then
    _LANGS="all"
  else
    _LANGS=$1
  fi
fi

################################################################
# Wiki Fasttext
EMBEDDING_FASTTEXT_WIKI_AR_DL="${BASE_URL}/fasttext/embeddings/wiki.ar.vec.zip"
EMBEDDING_FASTTEXT_WIKI_DE_DL="${BASE_URL}/fasttext/embeddings/wiki.de.vec.zip"
EMBEDDING_FASTTEXT_WIKI_EN_DL="${BASE_URL}/fasttext/embeddings/wiki.en.vec.zip"
EMBEDDING_FASTTEXT_WIKI_ES_DL="${BASE_URL}/fasttext/embeddings/wiki.es.vec.zip"
EMBEDDING_FASTTEXT_WIKI_FR_DL="${BASE_URL}/fasttext/embeddings/wiki.fr.vec.zip"
EMBEDDING_FASTTEXT_WIKI_IT_DL="${BASE_URL}/fasttext/embeddings/wiki.it.vec.zip"
EMBEDDING_FASTTEXT_WIKI_JA_DL="${BASE_URL}/fasttext/embeddings/wiki.ja.vec.zip"
EMBEDDING_FASTTEXT_WIKI_PT_DL="${BASE_URL}/fasttext/embeddings/wiki.pt.vec.zip"
EMBEDDING_FASTTEXT_WIKI_RU_DL="${BASE_URL}/fasttext/embeddings/wiki.ru.vec.zip"
EMBEDDING_FASTTEXT_WIKI_ZH_DL="${BASE_URL}/fasttext/embeddings/wiki.zh.vec.zip"
################################################################

################################################################
# Verbose output
GDATE_START=$( date +%s )
echo "[$(date)]: Starting download of FastText prerequisites"
echo "[$(date)]: Downloading from URL ${BASE_URL}"
if [[ $_LANGS == *"ar"* ]] || [[ $_LANGS == *"Ar"* ]] || [[ $_LANGS == *"AR"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  echo " - You asked to download FastText AR language prerequisites"
  DOWNLOAD_FASTTEXT_AR=true
fi
if [[ $_LANGS == *"de"* ]] || [[ $_LANGS == *"De"* ]] || [[ $_LANGS == *"DE"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  echo " - You asked to download FastText DE language prerequisites"
  DOWNLOAD_FASTTEXT_DE=true
fi
if [[ $_LANGS == *"en"* ]] || [[ $_LANGS == *"En"* ]] || [[ $_LANGS == *"EN"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  echo " - You asked to download FastText EN language prerequisites"
  DOWNLOAD_FASTTEXT_EN=true
fi
if [[ $_LANGS == *"es"* ]] || [[ $_LANGS == *"Es"* ]] || [[ $_LANGS == *"ES"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  echo " - You asked to download FastText ES language prerequisites"
  DOWNLOAD_FASTTEXT_ES=true
fi
if [[ $_LANGS == *"fr"* ]] || [[ $_LANGS == *"Fr"* ]] || [[ $_LANGS == *"FR"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  echo " - You asked to download FastText FR language prerequisites"
  DOWNLOAD_FASTTEXT_FR=true
fi
if [[ $_LANGS == *"it"* ]] || [[ $_LANGS == *"It"* ]] || [[ $_LANGS == *"IT"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  echo " - You asked to download FastText IT language prerequisites"
  DOWNLOAD_FASTTEXT_IT=true
fi
if [[ $_LANGS == *"ja"* ]] || [[ $_LANGS == *"Ja"* ]] || [[ $_LANGS == *"JA"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  echo " - You asked to download FastText JA language prerequisites"
  DOWNLOAD_FASTTEXT_JA=true
fi
if [[ $_LANGS == *"pt"* ]] || [[ $_LANGS == *"Pt"* ]] || [[ $_LANGS == *"PT"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  echo " - You asked to download FastText PT language prerequisites"
  DOWNLOAD_FASTTEXT_PT=true
fi
if [[ $_LANGS == *"ru"* ]] || [[ $_LANGS == *"Ru"* ]] || [[ $_LANGS == *"RU"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  echo " - You asked to download FastText RU language prerequisites"
  DOWNLOAD_FASTTEXT_RU=true
fi
if [[ $_LANGS == *"zh"* ]] || [[ $_LANGS == *"Zh"* ]] || [[ $_LANGS == *"ZH"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  echo " - You asked to download FastText ZH language prerequisites"
  DOWNLOAD_FASTTEXT_ZH=true
fi
################################################################

#-------- EMBEDDING --------
# wiki.ar.vec
if [ "${DOWNLOAD_FASTTEXT_AR}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of FastText EMBEDDING AR prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_AR_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting FastText EMBEDDING wiki.vec AR prerequisites [${EMBEDDING_FASTTEXT_WIKI_AR_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_AR_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-FastText EMBEDDING wiki.vec AR prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of FastText EMBEDDING AR prerequisites [${ELAPSED}]"
fi

# wiki.de.vec
if [ "${DOWNLOAD_FASTTEXT_DE}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of FastText EMBEDDING DE prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_DE_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting FastText EMBEDDING wiki.vec DE prerequisites [${EMBEDDING_FASTTEXT_WIKI_DE_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_DE_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-FastText EMBEDDING wiki.vec DE prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of FastText EMBEDDING DE prerequisites [${ELAPSED}]"
fi

# wiki.en.vec
if [ "${DOWNLOAD_FASTTEXT_EN}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of FastText EMBEDDING EN prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_EN_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting FastText EMBEDDING wiki.vec EN prerequisites [${EMBEDDING_FASTTEXT_WIKI_EN_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_EN_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-FastText EMBEDDING wiki.vec EN prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of FastText EMBEDDING EN prerequisites [${ELAPSED}]"
fi

# wiki.es.vec
if [ "${DOWNLOAD_FASTTEXT_ES}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of FastText EMBEDDING ES prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_ES_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting FastText EMBEDDING wiki.vec ES prerequisites [${EMBEDDING_FASTTEXT_WIKI_ES_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_ES_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-FastText EMBEDDING wiki.vec ES prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of FastText EMBEDDING ES prerequisites [${ELAPSED}]"
fi

# wiki.fr.vec
if [ "${DOWNLOAD_FASTTEXT_FR}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of FastText EMBEDDING FR prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_FR_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting FastText EMBEDDING wiki.vec FR prerequisites [${EMBEDDING_FASTTEXT_WIKI_FR_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_FR_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-FastText EMBEDDING wiki.vec FR prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of FastText EMBEDDING FR prerequisites [${ELAPSED}]"
fi

# wiki.it.vec
if [ "${DOWNLOAD_FASTTEXT_IT}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of FastText EMBEDDING IT prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_IT_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting FastText EMBEDDING wiki.vec IT prerequisites [${EMBEDDING_FASTTEXT_WIKI_IT_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_IT_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-FastText EMBEDDING wiki.vec IT prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of FastText EMBEDDING IT prerequisites [${ELAPSED}]"
fi

# wiki.ja.vec
if [ "${DOWNLOAD_FASTTEXT_JA}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of FastText EMBEDDING JA prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_JA_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting FastText EMBEDDING wiki.vec JA prerequisites [${EMBEDDING_FASTTEXT_WIKI_JA_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_JA_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-FastText EMBEDDING wiki.vec JA prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of FastText EMBEDDING JA prerequisites [${ELAPSED}]"
fi

# wiki.pt.vec
if [ "${DOWNLOAD_FASTTEXT_PT}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of FastText EMBEDDING PT prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_PT_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting FastText EMBEDDING wiki.vec PT prerequisites [${EMBEDDING_FASTTEXT_WIKI_PT_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_PT_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-FastText EMBEDDING wiki.vec PT prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of FastText EMBEDDING PT prerequisites [${ELAPSED}]"
fi

# wiki.ru.vec
if [ "${DOWNLOAD_FASTTEXT_RU}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of FastText EMBEDDING RU prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_RU_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting FastText EMBEDDING wiki.vec RU prerequisites [${EMBEDDING_FASTTEXT_WIKI_RU_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_RU_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-FastText EMBEDDING wiki.vec RU prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of FastText EMBEDDING RU prerequisites [${ELAPSED}]"
fi

# wiki.zh.vec
if [ "${DOWNLOAD_FASTTEXT_ZH}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of FastText EMBEDDING ZH prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_ZH_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting FastText EMBEDDING wiki.vec ZH prerequisites [${EMBEDDING_FASTTEXT_WIKI_ZH_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_ZH_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-FastText EMBEDDING wiki.vec ZH prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of FastText EMBEDDING ZH prerequisites [${ELAPSED}]"
fi
#-------- EMBEDDING --------

# Give permissions to workdir
chown -R kairntech:kairntech "${PATH_EMBEDDINGS}"

GDATE_END=$( date +%s )
DIFF_SECONDS=$(( GDATE_END - GDATE_START ))
GELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
echo "[$(date)]: Ending download of FastText prerequisites [${GELAPSED}]"
