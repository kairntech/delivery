#!/bin/bash

################################################################
DOWNLOAD_DELFT_DE=false
DOWNLOAD_DELFT_EN=false
DOWNLOAD_DELFT_ES=false
DOWNLOAD_DELFT_FR=false
DOWNLOAD_DELFT_IT=false
DOWNLOAD_DELFT_NL=false
INSTALL_ELMO=true
INSTALL_BERT=true
################################################################
PATH_EMBEDDINGS=/data/embeddings
PATH_LMDB=/data/db/db
################################################################

################################################################
# ELMO
ELMO_DE_FILES="elmo_de_options.json elmo_de_weights.hdf5"
ELMO_EN_FILES="elmo_2x4096_512_2048cnn_2xhighway_5.5B_options.json elmo_2x4096_512_2048cnn_2xhighway_5.5B_weights.hdf5"
ELMO_FR_FILES="elmo_fr_020_options.json elmo_fr_020_weights.hdf5"
# BERT
BERT_EN_FOLDER="cased_L-12_H-768_A-12" #folder
BERT_MULTI_FOLDER="multi_cased_L-12_H-768_A-12" #folder
################################################################
################################################################
LMDB_WIKI_DE_FOLDER="wiki.de"
LMDB_WIKI_EN_FOLDER="glove-840B"
LMDB_WIKI_ES_FOLDER="wiki.es"
LMDB_WIKI_FR_FOLDER="wiki.fr"
LMDB_WIKI_IT_FOLDER="wiki.it"
LMDB_WIKI_NL_FOLDER="wiki.nl"
################################################################

################################################################
# Catch environment
if [ -z "$1" ] ; then
  echo " - You need to add parameters such as $0 en to deploy Delft EN prerequisites, exiting.."
  exit 0
elif [ "$1" == "h" ] || [ "$1" == "-h" ] || [ "$1" == "help" ] || [ "$1" == "--help" ] ; then
  echo " - Usage of $0 is:"
  echo " ---  $0 en                       --- to deploy Delft EN prerequisites"
  echo " ---  $0 de,en                    --- to deploy Delft DE + EN prerequisites"
  echo " ---  $0 all                      --- to deploy all Delft prerequisites"
  echo " ---  $0 http://localhost all     --- to deploy all Delft prerequisites, from http://localhost"
  exit 0
fi

# Manage parameters
BASE_URL="https://kairntech.s3.eu-west-3.amazonaws.com"
if [[ $1 == *"http"* ]] ; then
  BASE_URL=$1
  if [ -z "$2" ] || [ "$2" == "" ] ; then
    _LANGS="all"
  else
    _LANGS=$2
  fi
else
  if [ -z "$1" ] || [ "$1" == "" ] ; then
    _LANGS="all"
  else
    _LANGS=$1
  fi
fi

################################################################
# ELMO
EMBEDDING_ELMO_DE_DL="${BASE_URL}/delft/embeddings/ELMo/elmo_de.zip"
EMBEDDING_ELMO_EN_DL="${BASE_URL}/delft/embeddings/ELMo/elmo_en.zip"
EMBEDDING_ELMO_FR_DL="${BASE_URL}/delft/embeddings/ELMo/elmo_fr.zip"
# BERT
EMBEDDING_BERT_EN_DL="${BASE_URL}/delft/embeddings/BERT/bert-base-en.zip"
EMBEDDING_BERT_MULTI_DL="${BASE_URL}/delft/embeddings/BERT/bert-base-multi.zip"
################################################################
################################################################
LMDB_WIKI_DE_DL="${BASE_URL}/delft/lmdb/wiki.de.zip"
LMDB_WIKI_EN_DL="${BASE_URL}/delft/lmdb/glove-840B.zip"
LMDB_WIKI_ES_DL="${BASE_URL}/delft/lmdb/wiki.es.zip"
LMDB_WIKI_FR_DL="${BASE_URL}/delft/lmdb/wiki.fr.zip"
LMDB_WIKI_IT_DL="${BASE_URL}/delft/lmdb/wiki.it.zip"
LMDB_WIKI_NL_DL="${BASE_URL}/delft/lmdb/wiki.nl.zip"
################################################################

################################################################
# Verbose output
GDATE_START=$( date +%s )
echo "[$(date)]: Starting download of Delft prerequisites"
echo "[$(date)]: Downloading from URL ${BASE_URL}"
if [[ $_LANGS == *"de"* ]] || [[ $_LANGS == *"De"* ]] || [[ $_LANGS == *"DE"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  echo " - You asked to download Delft DE language prerequisites"
  DOWNLOAD_DELFT_DE=true
fi
if [[ $_LANGS == *"en"* ]] || [[ $_LANGS == *"En"* ]] || [[ $_LANGS == *"EN"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  echo " - You asked to download Delft EN language prerequisites"
  DOWNLOAD_DELFT_EN=true
fi
if [[ $_LANGS == *"es"* ]] || [[ $_LANGS == *"Es"* ]] || [[ $_LANGS == *"ES"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  echo " - You asked to download Delft ES language prerequisites"
  DOWNLOAD_DELFT_ES=true
fi
if [[ $_LANGS == *"fr"* ]] || [[ $_LANGS == *"Fr"* ]] || [[ $_LANGS == *"FR"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  echo " - You asked to download Delft FR language prerequisites"
  DOWNLOAD_DELFT_FR=true
fi
if [[ $_LANGS == *"it"* ]] || [[ $_LANGS == *"It"* ]] || [[ $_LANGS == *"IT"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  echo " - You asked to download Delft IT language prerequisites"
  DOWNLOAD_DELFT_IT=true
fi
if [[ $_LANGS == *"nl"* ]] || [[ $_LANGS == *"Nl"* ]] || [[ $_LANGS == *"NL"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  echo " - You asked to download Delft NL language prerequisites"
  DOWNLOAD_DELFT_NL=true
fi
################################################################

#-------- EMBEDDING --------
# ELMo DE
if [ "${DOWNLOAD_DELFT_DE}" == "true" ] ; then
  if [ "${INSTALL_ELMO}" == "true" ] ; then
    DATE_START=$( date +%s )
    echo " --[$(date)]: Starting deployment of Delft EMBEDDING ELMo DE prerequisites"
    EMBEDDING=true
    if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
      mkdir -p "${PATH_EMBEDDINGS}"
      EMBEDDING=false
    else
      for EMBEDDING in $ELMO_DE_FILES ; do
        if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
          EMBEDDING=false
        fi
      done
    fi
    if [ "${EMBEDDING}" == "false" ] ; then
      echo " --[$(date)]: |-Downloading and extracting Delft prerequisites [${EMBEDDING_ELMO_DE_DL}]"
      cd "${PATH_EMBEDDINGS}"
      wget -qO- "${EMBEDDING_ELMO_DE_DL}" | bsdtar -xf-
    else
      echo " --[$(date)]: |-Delft EMBEDDING ELMo DE prerequisite already satisfied"
    fi
    DATE_END=$( date +%s )
    DIFF_SECONDS=$(( DATE_END - DATE_START ))
    ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
    echo " --[$(date)]: Ending deployment of Delft EMBEDDING ELMo DE prerequisites [${ELAPSED}]"
  fi
fi

# ELMo EN
# BERT EN
if [ "${DOWNLOAD_DELFT_EN}" == "true" ] ; then
  if [ "${INSTALL_ELMO}" == "true" ] ; then
    DATE_START=$( date +%s )
    echo " --[$(date)]: Starting deployment of Delft EMBEDDING ELMo EN prerequisites"
    EMBEDDING=true
    if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
      mkdir -p "${PATH_EMBEDDINGS}"
      EMBEDDING=false
    else
      for EMBEDDING in $ELMO_EN_FILES ; do
        if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
          EMBEDDING=false
        fi
      done
    fi
    if [ "${EMBEDDING}" == "false" ] ; then
      echo " --[$(date)]: |-Downloading and extracting Delft prerequisites [${EMBEDDING_ELMO_EN_DL}]"
      cd "${PATH_EMBEDDINGS}"
      wget -qO- "${EMBEDDING_ELMO_EN_DL}" | bsdtar -xf-
    else
      echo " --[$(date)]: |-Delft EMBEDDING ELMo EN prerequisite already satisfied"
    fi
    DATE_END=$( date +%s )
    DIFF_SECONDS=$(( DATE_END - DATE_START ))
    ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
    echo " --[$(date)]: Ending deployment of Delft EMBEDDING ELMo EN prerequisites [${ELAPSED}]"
  fi
  if [ "${INSTALL_BERT}" == "true" ] ; then
    EMBEDDING=true 
    DATE_START=$( date +%s )
    echo " --[$(date)]: Starting deployment of Delft EMBEDDING BERT EN prerequisites"
    for EMBEDDING in $BERT_EN_FOLDER ; do
      if [ ! -d "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      elif [ ! -n "$( ls -A "${PATH_EMBEDDINGS}"/"${EMBEDDING}" )" ] ; then
        #rm -rf "${PATH_EMBEDDINGS}"/"${EMBEDDING}"
        EMBEDDING=false
      fi
    done
    if [ "${EMBEDDING}" == "false" ] ; then
      echo " --[$(date)]: |-Downloading and extracting Delft prerequisites [${EMBEDDING_BERT_EN_DL}]"
      cd "${PATH_EMBEDDINGS}"
      wget -qO- "${EMBEDDING_BERT_EN_DL}" | bsdtar -xf-
    else
      echo " --[$(date)]: |-Delft EMBEDDING BERT EN prerequisite already satisfied"
    fi
    DATE_END=$( date +%s )
    DIFF_SECONDS=$(( DATE_END - DATE_START ))
    ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
    echo " --[$(date)]: Ending deployment of Delft EMBEDDING BERT EN prerequisites [${ELAPSED}]"
  fi
fi

# ELMo FR
if [ "${DOWNLOAD_DELFT_FR}" == "true" ] ; then
  if [ "${INSTALL_ELMO}" == "true" ] ; then
    DATE_START=$( date +%s )
    echo " --[$(date)]: Starting deployment of Delft EMBEDDING ELMo FR prerequisites"
    EMBEDDING=true 
    if [ ! -d "${PATH_EMBEDDINGS}" ] ; then 
      mkdir -p "${PATH_EMBEDDINGS}"
      EMBEDDING=false
    else
      for EMBEDDING in $ELMO_FR_FILES ; do
        if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
          EMBEDDING=false
        fi
      done
    fi
    if [ "${EMBEDDING}" == "false" ] ; then
      echo " --[$(date)]: |-Downloading and extracting Delft prerequisites [${EMBEDDING_ELMO_FR_DL}]"
      cd "${PATH_EMBEDDINGS}"
      wget -qO- "${EMBEDDING_ELMO_FR_DL}" | bsdtar -xf-
    else
      echo " --[$(date)]: |-Delft EMBEDDING ELMo FR prerequisite already satisfied"
    fi
    DATE_END=$( date +%s )
    DIFF_SECONDS=$(( DATE_END - DATE_START ))
    ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
    echo " --[$(date)]: Ending deployment of Delft EMBEDDING ELMo FR prerequisites [${ELAPSED}]"
  fi
fi

# BERT MULTI
if [ "${INSTALL_BERT}" == "true" ] ; then
  EMBEDDING=true
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Delft EMBEDDING BERT MULTI prerequisites"
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $BERT_MULTI_FOLDER ; do
      if [ ! -d "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      elif [ ! -n "$( ls -A "${PATH_EMBEDDINGS}"/"${EMBEDDING}" )" ] ; then
        #rm -rf "${PATH_EMBEDDINGS}"/"${EMBEDDING}"
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    echo " --[$(date)]: |-Downloading and extracting Delft prerequisites [${EMBEDDING_BERT_MULTI_DL}]"
    cd "${PATH_EMBEDDINGS}"
    wget -qO- "${EMBEDDING_BERT_MULTI_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Delft EMBEDDING BERT MULTI prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Delft EMBEDDING BERT MULTI prerequisites [${ELAPSED}]"
fi
#-------- EMBEDDING --------

#-------- LMDB --------
# wiki.de
if [ "${DOWNLOAD_DELFT_DE}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Delft LMDB DE prerequisites"
  LMDB=true
  if [ ! -d "${PATH_LMDB}" ] ; then
    mkdir -p "${PATH_LMDB}"
    LMDB=false
  else
    for LMDB in $LMDB_WIKI_DE_FOLDER ; do
      if [ ! -d "${PATH_LMDB}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_LMDB}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_LMDB}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    echo " --[$(date)]: |-Downloading and extracting Delft prerequisites [${LMDB_WIKI_DE_DL}]"
    cd "${PATH_LMDB}"
    wget -qO- "${LMDB_WIKI_DE_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Delft LMDB DE prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Delft LMDB DE prerequisites [${ELAPSED}]"
fi

# glove-840B
if [ "${DOWNLOAD_DELFT_EN}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Delft LMDB EN prerequisites"
  LMDB=true
  if [ ! -d "${PATH_LMDB}" ] ; then
    mkdir -p "${PATH_LMDB}"
    LMDB=false
  else
    for LMDB in $LMDB_WIKI_EN_FOLDER ; do
      if [ ! -d "${PATH_LMDB}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_LMDB}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_LMDB}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    echo " --[$(date)]: |-Downloading and extracting Delft prerequisites [${LMDB_WIKI_EN_DL}]"
    cd "${PATH_LMDB}"
    wget -qO- "${LMDB_WIKI_EN_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Delft LMDB EN prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Delft LMDB EN prerequisites [${ELAPSED}]"
fi

# wiki.es
if [ "${DOWNLOAD_DELFT_ES}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Delft LMDB ES prerequisites"
  LMDB=true
  if [ ! -d "${PATH_LMDB}" ] ; then
    mkdir -p "${PATH_LMDB}"
    LMDB=false
  else
    for LMDB in $LMDB_WIKI_ES_FOLDER ; do
      if [ ! -d "${PATH_LMDB}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_LMDB}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_LMDB}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    echo " --[$(date)]: |-Downloading and extracting Delft prerequisites [${LMDB_WIKI_ES_DL}]"
    cd "${PATH_LMDB}"
    wget -qO- "${LMDB_WIKI_ES_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Delft LMDB ES prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Delft LMDB ES prerequisites [${ELAPSED}]"
fi

# wiki.fr
if [ "${DOWNLOAD_DELFT_FR}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Delft LMDB FR prerequisites"
  LMDB=true
  if [ ! -d "${PATH_LMDB}" ] ; then
    mkdir -p "${PATH_LMDB}"
    LMDB=false
  else
    for LMDB in $LMDB_WIKI_FR_FOLDER ; do
      if [ ! -d "${PATH_LMDB}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_LMDB}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_LMDB}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    echo " --[$(date)]: |-Downloading and extracting Delft prerequisites [${LMDB_WIKI_FR_DL}]"
    cd "${PATH_LMDB}"
    wget -qO- "${LMDB_WIKI_FR_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Delft LMDB FR prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Delft LMDB FR prerequisites [${ELAPSED}]"
fi

# wiki.it
if [ "${DOWNLOAD_DELFT_IT}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Delft LMDB IT prerequisites"
  LMDB=true
  if [ ! -d "${PATH_LMDB}" ] ; then
    mkdir -p "${PATH_LMDB}"
    LMDB=false
  else
    for LMDB in $LMDB_WIKI_IT_FOLDER ; do
      if [ ! -d "${PATH_LMDB}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_LMDB}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_LMDB}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    echo " --[$(date)]: |-Downloading and extracting Delft prerequisites [${LMDB_WIKI_IT_DL}]"
    cd "${PATH_LMDB}"
    wget -qO- "${LMDB_WIKI_IT_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Delft LMDB IT prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Delft LMDB IT prerequisites [${ELAPSED}]"
fi

# wiki.nl
if [ "${DOWNLOAD_DELFT_NL}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Delft LMDB NL prerequisites"
  LMDB=true
  if [ ! -d "${PATH_LMDB}" ] ; then
    mkdir -p "${PATH_LMDB}"
    LMDB=false
  else
    for LMDB in $LMDB_WIKI_NL_FOLDER ; do
      if [ ! -d "${PATH_LMDB}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_LMDB}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_LMDB}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    echo " --[$(date)]: |-Downloading and extracting Delft prerequisites [${LMDB_WIKI_NL_DL}]"
    cd "${PATH_LMDB}"
    wget -qO- "${LMDB_WIKI_NL_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Delft LMDB NL prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Delft LMDB NL prerequisites [${ELAPSED}]"
fi
#-------- LMDB --------

# Give permissions to workdir
chown -R kairntech:kairntech "${PATH_EMBEDDINGS}" "${PATH_LMDB}"

GDATE_END=$( date +%s )
DIFF_SECONDS=$(( GDATE_END - GDATE_START ))
GELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
echo "[$(date)]: Ending download of Delft prerequisites [${GELAPSED}]"
