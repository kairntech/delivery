#!/bin/bash
  
################################################################
DOWNLOAD_FLAIR_DE=false
DOWNLOAD_FLAIR_EN=false
DOWNLOAD_FLAIR_ES=false
DOWNLOAD_FLAIR_FA=false
DOWNLOAD_FLAIR_FR=false
DOWNLOAD_FLAIR_IT=false
DOWNLOAD_FLAIR_JA=false
DOWNLOAD_FLAIR_NL=false
DOWNLOAD_FLAIR_AR=false
DOWNLOAD_FLAIR_HI=false
DOWNLOAD_FLAIR_ZH=false
DOWNLOAD_FLAIR_RU=false
DOWNLOAD_FLAIR_PT=false
DOWNLOAD_FLAIR_XX=true
################################################################
PATH_EMBEDDINGS=/data/embeddings
PATH_DATASET=/data/datasets
FLAIR_DATE="04-27-2023"
################################################################

################################################################
# Wiki Fasttext
EMBEDDING_FASTTEXT_WIKI_DE_FILES="de-wiki-fasttext-300d-1M.vectors.npy"
EMBEDDING_FASTTEXT_WIKI_ES_FILES="es-wiki-fasttext-300d-1M.vectors.npy"
EMBEDDING_FASTTEXT_WIKI_FA_FILES="fa-wiki-fasttext-300d-1M.vectors.npy"
EMBEDDING_FASTTEXT_WIKI_FR_FILES="fr-wiki-fasttext-300d-1M.vectors.npy"
EMBEDDING_FASTTEXT_WIKI_IT_FILES="it-wiki-fasttext-300d-1M.vectors.npy"
EMBEDDING_FASTTEXT_WIKI_JA_FILES="ja-wiki-fasttext-300d-1M.vectors.npy"
EMBEDDING_FASTTEXT_WIKI_NL_FILES="nl-wiki-fasttext-300d-1M.vectors.npy"
EMBEDDING_FASTTEXT_WIKI_AR_FILES="ar-wiki-fasttext-300d-1M.vectors.npy"
EMBEDDING_FASTTEXT_WIKI_HI_FILES="hi-wiki-fasttext-300d-1M.vectors.npy"
EMBEDDING_FASTTEXT_WIKI_ZH_FILES="zh-wiki-fasttext-300d-1M.vectors.npy"
EMBEDDING_FASTTEXT_WIKI_RU_FILES="ru-wiki-fasttext-300d-1M.vectors.npy"
EMBEDDING_FASTTEXT_WIKI_PT_FILES="pt-wiki-fasttext-300d-1M.vectors.npy"
# News Fasttext
EMBEDDING_FASTTEXT_NEWS_EN_FILES="en-fasttext-news-300d-1M.vectors.npy"
# ForwardBackward LM
EMBEDDING_FWBW_LM_DE_FILES="lm-mix-german-backward-v0.2rc.pt lm-mix-german-forward-v0.2rc.pt"
EMBEDDING_FWBW_LM_EN_FILES="news-backward-0.4.1.pt news-forward-0.4.1.pt lm-news-english-backward-1024-v0.2rc.pt lm-news-english-forward-1024-v0.2rc.pt"
EMBEDDING_FWBW_LM_ES_FILES="lm-es-backward.pt lm-es-forward.pt"
EMBEDDING_FWBW_LM_FA_FILES="lm-fa-opus-large-backward-v0.1.pt lm-fa-opus-large-forward-v0.1.pt"
EMBEDDING_FWBW_LM_FR_FILES="lm-fr-charlm-backward.pt lm-fr-charlm-forward.pt"
EMBEDDING_FWBW_LM_IT_FILES="lm-it-opus-large-backward-v0.1.pt lm-it-opus-large-forward-v0.1.pt"
EMBEDDING_FWBW_LM_JA_FILES="japanese-backward.pt japanese-forward.pt"
EMBEDDING_FWBW_LM_NL_FILES="lm-nl-opus-large-backward-v0.1.pt lm-nl-opus-large-forward-v0.1.pt"
EMBEDDING_FWBW_LM_AR_FILES="lm-ar-opus-large-backward-v0.1.pt lm-ar-opus-large-forward-v0.1.pt"
EMBEDDING_FWBW_LM_PT_FILES="lm-pt-backward.pt lm-pt-forward.pt"
EMBEDDING_FWBW_LM_XX_FILES="lm-jw300-backward-v0.1.pt lm-jw300-forward-v0.1.pt"
# Bytepair
EMBEDDING_BYTEPAIR_DE_FILES="de.wiki.bpe.vs100000.model de.wiki.bpe.vs100000.d50.w2v.bin"
EMBEDDING_BYTEPAIR_EN_FILES="en.wiki.bpe.vs100000.model en.wiki.bpe.vs100000.d50.w2v.bin"
EMBEDDING_BYTEPAIR_ES_FILES="es.wiki.bpe.vs100000.model es.wiki.bpe.vs100000.d50.w2v.bin"
EMBEDDING_BYTEPAIR_FA_FILES="fa.wiki.bpe.vs100000.model fa.wiki.bpe.vs100000.d50.w2v.bin"
EMBEDDING_BYTEPAIR_FR_FILES="fr.wiki.bpe.vs100000.model fr.wiki.bpe.vs100000.d50.w2v.bin"
EMBEDDING_BYTEPAIR_IT_FILES="it.wiki.bpe.vs100000.model it.wiki.bpe.vs100000.d50.w2v.bin"
EMBEDDING_BYTEPAIR_JA_FILES="ja.wiki.bpe.vs100000.model ja.wiki.bpe.vs100000.d50.w2v.bin"
EMBEDDING_BYTEPAIR_NL_FILES="nl.wiki.bpe.vs100000.model nl.wiki.bpe.vs100000.d50.w2v.bin"
EMBEDDING_BYTEPAIR_AR_FILES="ar.wiki.bpe.vs100000.model ar.wiki.bpe.vs100000.d50.w2v.bin"
EMBEDDING_BYTEPAIR_ZH_FILES="zh.wiki.bpe.vs100000.model zh.wiki.bpe.vs100000.d50.w2v.bin"
EMBEDDING_BYTEPAIR_RU_FILES="ru.wiki.bpe.vs100000.model ru.wiki.bpe.vs100000.d50.w2v.bin"
EMBEDDING_BYTEPAIR_PT_FILES="pt.wiki.bpe.vs100000.model pt.wiki.bpe.vs100000.d50.w2v.bin"
EMBEDDING_BYTEPAIR_XX_FILES="multi.wiki.bpe.vs100000.model multi.wiki.bpe.vs100000.d300.w2v.bin"
################################################################
################################################################
LMDB_WIKI_DE_FOLDER="de-wiki-fasttext-300d-1M.lmdb"
LMDB_NEWS_EN_FOLDER="en-fasttext-news-300d-1M.lmdb"
LMDB_WIKI_ES_FOLDER="es-wiki-fasttext-300d-1M.lmdb"
LMDB_WIKI_FA_FOLDER="fa-wiki-fasttext-300d-1M.lmdb"
LMDB_WIKI_FR_FOLDER="fr-wiki-fasttext-300d-1M.lmdb"
LMDB_WIKI_IT_FOLDER="it-wiki-fasttext-300d-1M.lmdb"
LMDB_WIKI_JA_FOLDER="ja-wiki-fasttext-300d-1M.lmdb"
LMDB_WIKI_NL_FOLDER="nl-wiki-fasttext-300d-1M.lmdb"
LMDB_WIKI_AR_FOLDER="ar-wiki-fasttext-300d-1M.lmdb"
LMDB_WIKI_HI_FOLDER="hi-wiki-fasttext-300d-1M.lmdb"
LMDB_WIKI_ZH_FOLDER="zh-wiki-fasttext-300d-1M.lmdb"
LMDB_WIKI_RU_FOLDER="ru-wiki-fasttext-300d-1M.lmdb"
LMDB_WIKI_PT_FOLDER="pt-wiki-fasttext-300d-1M.lmdb"
################################################################
################################################################
# dataset common_characters
DATASET_COMMON_CHARACTERS="common_characters"
################################################################

################################################################
# Catch environment
if [ -z "$1" ] ; then
  echo " - You need to add parameters such as $0 en to deploy Flair EN prerequisites, exiting.."
  exit 0
elif [ "$1" == "h" ] || [ "$1" == "-h" ] || [ "$1" == "help" ] || [ "$1" == "--help" ] ; then
  echo " - Usage of $0 is:"
  echo " ---  $0 en                                 --- to deploy Flair EN prerequisites"
  echo " ---  $0 de,en                              --- to deploy Flair DE + EN prerequisites"
  echo " ---  $0 all                                --- to deploy all Flair prerequisites"
  echo " ---  $0 all 12-17-2022                     --- to deploy all Flair prerequisites, dated 17th December, 2022"
  echo " ---  $0 http://localhost all 12-17-2022    --- to deploy all Flair prerequisites, from http://localhost, dated 17th December, 2022"
  exit 0
fi

# Manage parameters
BASE_URL="https://kairntech.s3.eu-west-3.amazonaws.com"
if [[ $1 == *"http"* ]] ; then
  BASE_URL=$1
  if [ -z "$2" ] || [ "$2" == "" ] ; then
    _LANGS="all"
  else
    _LANGS=$2
  fi
  if [ -z "$3" ] || [ "$3" == "" ] ; then
    _DATE=${FLAIR_DATE}
  else
    _DATE=$3
  fi
else
  if [ -z "$1" ] || [ "$1" == "" ] ; then
    _LANGS="all"
  else
    _LANGS=$1
  fi
  if [ -z "$2" ] || [ "$2" == "" ] ; then
    _DATE=${FLAIR_DATE}
  else
    _DATE=$2
  fi
fi

################################################################
# Wiki Fasttext
EMBEDDING_FASTTEXT_WIKI_DE_DL="${BASE_URL}/flair/embeddings/FastText/fasttext_wiki_de.zip"
EMBEDDING_FASTTEXT_WIKI_ES_DL="${BASE_URL}/flair/embeddings/FastText/fasttext_wiki_es.zip"
EMBEDDING_FASTTEXT_WIKI_FA_DL="${BASE_URL}/flair/embeddings/FastText/fasttext_wiki_fa.zip"
EMBEDDING_FASTTEXT_WIKI_FR_DL="${BASE_URL}/flair/embeddings/FastText/fasttext_wiki_fr.zip"
EMBEDDING_FASTTEXT_WIKI_IT_DL="${BASE_URL}/flair/embeddings/FastText/fasttext_wiki_it.zip"
EMBEDDING_FASTTEXT_WIKI_JA_DL="${BASE_URL}/flair/embeddings/FastText/fasttext_wiki_ja.zip"
EMBEDDING_FASTTEXT_WIKI_NL_DL="${BASE_URL}/flair/embeddings/FastText/fasttext_wiki_nl.zip"
EMBEDDING_FASTTEXT_WIKI_AR_DL="${BASE_URL}/flair/embeddings/FastText/fasttext_wiki_ar.zip"
EMBEDDING_FASTTEXT_WIKI_HI_DL="${BASE_URL}/flair/embeddings/FastText/fasttext_wiki_hi.zip"
EMBEDDING_FASTTEXT_WIKI_ZH_DL="${BASE_URL}/flair/embeddings/FastText/fasttext_wiki_zh.zip"
EMBEDDING_FASTTEXT_WIKI_RU_DL="${BASE_URL}/flair/embeddings/FastText/fasttext_wiki_ru.zip"
EMBEDDING_FASTTEXT_WIKI_PT_DL="${BASE_URL}/flair/embeddings/FastText/fasttext_wiki_pt.zip"
# News Fasttext
EMBEDDING_FASTTEXT_NEWS_EN_DL="${BASE_URL}/flair/embeddings/FastText/fasttext_news_en.zip"
# ForwardBackward LM
EMBEDDING_FWBW_LM_DE_DL="${BASE_URL}/flair/embeddings/ForwardBackward/fwbw-lm-de.zip"
EMBEDDING_FWBW_LM_EN_DL="${BASE_URL}/flair/embeddings/ForwardBackward/fwbw-lm-en.zip"
EMBEDDING_FWBW_LM_ES_DL="${BASE_URL}/flair/embeddings/ForwardBackward/fwbw-lm-es.zip"
EMBEDDING_FWBW_LM_FA_DL="${BASE_URL}/flair/embeddings/ForwardBackward/fwbw-lm-fa.zip"
EMBEDDING_FWBW_LM_FR_DL="${BASE_URL}/flair/embeddings/ForwardBackward/fwbw-lm-fr.zip"
EMBEDDING_FWBW_LM_IT_DL="${BASE_URL}/flair/embeddings/ForwardBackward/fwbw-lm-it.zip"
EMBEDDING_FWBW_LM_JA_DL="${BASE_URL}/flair/embeddings/ForwardBackward/fwbw-lm-ja.zip"
EMBEDDING_FWBW_LM_NL_DL="${BASE_URL}/flair/embeddings/ForwardBackward/fwbw-lm-nl.zip"
EMBEDDING_FWBW_LM_AR_DL="${BASE_URL}/flair/embeddings/ForwardBackward/fwbw-lm-ar.zip"
EMBEDDING_FWBW_LM_PT_DL="${BASE_URL}/flair/embeddings/ForwardBackward/fwbw-lm-pt.zip"
EMBEDDING_FWBW_LM_XX_DL="${BASE_URL}/flair/embeddings/ForwardBackward/fwbw-lm-xx.zip"
# Bytepair
EMBEDDING_BYTEPAIR_DE_DL="${BASE_URL}/flair/embeddings/Bytepair/bytepair_wiki_de.zip"
EMBEDDING_BYTEPAIR_EN_DL="${BASE_URL}/flair/embeddings/Bytepair/bytepair_wiki_en.zip"
EMBEDDING_BYTEPAIR_ES_DL="${BASE_URL}/flair/embeddings/Bytepair/bytepair_wiki_es.zip"
EMBEDDING_BYTEPAIR_FA_DL="${BASE_URL}/flair/embeddings/Bytepair/bytepair_wiki_fa.zip"
EMBEDDING_BYTEPAIR_FR_DL="${BASE_URL}/flair/embeddings/Bytepair/bytepair_wiki_fr.zip"
EMBEDDING_BYTEPAIR_IT_DL="${BASE_URL}/flair/embeddings/Bytepair/bytepair_wiki_it.zip"
EMBEDDING_BYTEPAIR_JA_DL="${BASE_URL}/flair/embeddings/Bytepair/bytepair_wiki_ja.zip"
EMBEDDING_BYTEPAIR_NL_DL="${BASE_URL}/flair/embeddings/Bytepair/bytepair_wiki_nl.zip"
EMBEDDING_BYTEPAIR_AR_DL="${BASE_URL}/flair/embeddings/Bytepair/bytepair_wiki_ar.zip"
EMBEDDING_BYTEPAIR_RU_DL="${BASE_URL}/flair/embeddings/Bytepair/bytepair_wiki_ru.zip"
EMBEDDING_BYTEPAIR_PT_DL="${BASE_URL}/flair/embeddings/Bytepair/bytepair_wiki_pt.zip"
EMBEDDING_BYTEPAIR_ZH_DL="${BASE_URL}/flair/embeddings/Bytepair/bytepair_wiki_zh.zip"
EMBEDDING_BYTEPAIR_XX_DL="${BASE_URL}/flair/embeddings/Bytepair/bytepair_wiki_xx.zip"
# dataset common_characters
DATASET_COMMON_CHARACTERS_DL="${BASE_URL}/flair/datasets/common_characters.zip"
################################################################
################################################################
LMDB_NEWS_EN_DL="${BASE_URL}/flair/lmdb/${_DATE}/en-fasttext-news-300d-1M.lmdb.zip"
LMDB_WIKI_DE_DL="${BASE_URL}/flair/lmdb/${_DATE}/de-wiki-fasttext-300d-1M.lmdb.zip"
LMDB_WIKI_ES_DL="${BASE_URL}/flair/lmdb/${_DATE}/es-wiki-fasttext-300d-1M.lmdb.zip"
LMDB_WIKI_FA_DL="${BASE_URL}/flair/lmdb/${_DATE}/fa-wiki-fasttext-300d-1M.lmdb.zip"
LMDB_WIKI_FR_DL="${BASE_URL}/flair/lmdb/${_DATE}/fr-wiki-fasttext-300d-1M.lmdb.zip"
LMDB_WIKI_IT_DL="${BASE_URL}/flair/lmdb/${_DATE}/it-wiki-fasttext-300d-1M.lmdb.zip"
LMDB_WIKI_JA_DL="${BASE_URL}/flair/lmdb/${_DATE}/ja-wiki-fasttext-300d-1M.lmdb.zip"
LMDB_WIKI_NL_DL="${BASE_URL}/flair/lmdb/${_DATE}/nl-wiki-fasttext-300d-1M.lmdb.zip"
LMDB_WIKI_AR_DL="${BASE_URL}/flair/lmdb/${_DATE}/ar-wiki-fasttext-300d-1M.lmdb.zip"
LMDB_WIKI_HI_DL="${BASE_URL}/flair/lmdb/${_DATE}/hi-wiki-fasttext-300d-1M.lmdb.zip"
LMDB_WIKI_ZH_DL="${BASE_URL}/flair/lmdb/${_DATE}/zh-wiki-fasttext-300d-1M.lmdb.zip"
LMDB_WIKI_RU_DL="${BASE_URL}/flair/lmdb/${_DATE}/ru-wiki-fasttext-300d-1M.lmdb.zip"
LMDB_WIKI_PT_DL="${BASE_URL}/flair/lmdb/${_DATE}/pt-wiki-fasttext-300d-1M.lmdb.zip"
################################################################

################################################################
# Verbose output
GDATE_START=$( date +%s )
echo "[$(date)]: Starting download of Flair prerequisites"
echo "[$(date)]: Downloading from URL ${BASE_URL}"
if [[ $_LANGS == *"de"* ]] || [[ $_LANGS == *"De"* ]] || [[ $_LANGS == *"DE"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_FLAIR_DE=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download Flair DE language prerequisites"
  else
    echo " - You asked to download Flair DE language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"en"* ]] || [[ $_LANGS == *"En"* ]] || [[ $_LANGS == *"EN"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_FLAIR_EN=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download Flair EN language prerequisites"
  else
    echo " - You asked to download Flair EN language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"es"* ]] || [[ $_LANGS == *"Es"* ]] || [[ $_LANGS == *"ES"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_FLAIR_ES=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download Flair ES language prerequisites"
  else
    echo " - You asked to download Flair ES language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"fa"* ]] || [[ $_LANGS == *"Fa"* ]] || [[ $_LANGS == *"FA"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_FLAIR_FA=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download Flair FA language prerequisites"
  else
    echo " - You asked to download Flair FA language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"fr"* ]] || [[ $_LANGS == *"Fr"* ]] || [[ $_LANGS == *"FR"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_FLAIR_FR=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download Flair FR language prerequisites"
  else
    echo " - You asked to download Flair FR language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"it"* ]] || [[ $_LANGS == *"It"* ]] || [[ $_LANGS == *"IT"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_FLAIR_IT=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download Flair IT language prerequisites"
  else
    echo " - You asked to download Flair IT language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"ja"* ]] || [[ $_LANGS == *"Ja"* ]] || [[ $_LANGS == *"JA"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_FLAIR_JA=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download Flair JA language prerequisites"
  else
    echo " - You asked to download Flair JA language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"nl"* ]] || [[ $_LANGS == *"Nl"* ]] || [[ $_LANGS == *"NL"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_FLAIR_NL=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download Flair NL language prerequisites"
  else
    echo " - You asked to download Flair NL language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"ar"* ]] || [[ $_LANGS == *"Ar"* ]] || [[ $_LANGS == *"AR"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_FLAIR_AR=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download Flair AR language prerequisites"
  else
    echo " - You asked to download Flair AR language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"hi"* ]] || [[ $_LANGS == *"Hi"* ]] || [[ $_LANGS == *"HI"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_FLAIR_HI=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download Flair HI language prerequisites"
  else
    echo " - You asked to download Flair HI language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"zh"* ]] || [[ $_LANGS == *"Zh"* ]] || [[ $_LANGS == *"ZH"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_FLAIR_ZH=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download Flair ZH language prerequisites"
  else
    echo " - You asked to download Flair ZH language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"ru"* ]] || [[ $_LANGS == *"Ru"* ]] || [[ $_LANGS == *"RU"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_FLAIR_RU=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download Flair RU language prerequisites"
  else
    echo " - You asked to download Flair RU language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"pt"* ]] || [[ $_LANGS == *"Pt"* ]] || [[ $_LANGS == *"PT"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_FLAIR_PT=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download Flair PT language prerequisites"
  else
    echo " - You asked to download Flair PT language prerequisites, dated $_DATE"
  fi
fi
################################################################

#-------- EMBEDDING --------
# de-wiki-fasttext-300d-1M.vectors.npy
# ForwardBackward LM-DE
if [ "${DOWNLOAD_FLAIR_DE}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair EMBEDDING DE prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_DE_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING wiki-Fasttext DE prerequisites [${EMBEDDING_FASTTEXT_WIKI_DE_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_DE_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING wiki-Fasttext DE prerequisite already satisfied"
  fi
  EMBEDDING=true
  for EMBEDDING in $EMBEDDING_FWBW_LM_DE_FILES ; do
    if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
      EMBEDDING=false
    fi
  done
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING ForwardBackward DE prerequisites [${EMBEDDING_FWBW_LM_DE_DL}]"
    wget -qO- "${EMBEDDING_FWBW_LM_DE_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING ForwardBackward DE prerequisite already satisfied"
  fi
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}"/de ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"/de
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_BYTEPAIR_DE_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/de/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"/de
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING Bytepair DE prerequisites [${EMBEDDING_BYTEPAIR_DE_DL}]"
    wget -qO- "${EMBEDDING_BYTEPAIR_DE_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING Bytepair DE prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair EMBEDDING DE prerequisites [${ELAPSED}]"
fi

# en-fasttext-news-300d-1M.vectors.npy
# ForwardBackward LM-EN
if [ "${DOWNLOAD_FLAIR_EN}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair EMBEDDING EN prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_NEWS_EN_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING news-Fasttext EN prerequisites [${EMBEDDING_FASTTEXT_NEWS_EN_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_NEWS_EN_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING news-Fasttext EN prerequisite already satisfied"
  fi
  EMBEDDING=true
  for EMBEDDING in $EMBEDDING_FWBW_LM_EN_FILES ; do
    if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
      EMBEDDING=false
    fi
  done
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING ForwardBackward EN prerequisites [${EMBEDDING_FWBW_LM_EN_DL}]"
    wget -qO- "${EMBEDDING_FWBW_LM_EN_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING ForwardBackward EN prerequisite already satisfied"
  fi
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}"/en ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"/en
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_BYTEPAIR_EN_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/en/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"/en
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING Bytepair EN prerequisites [${EMBEDDING_BYTEPAIR_EN_DL}]"
    wget -qO- "${EMBEDDING_BYTEPAIR_EN_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING Bytepair EN prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair EMBEDDING EN prerequisites [${ELAPSED}]"
fi

# es-wiki-fasttext-300d-1M.vectors.npy
# ForwardBackward LM-ES
if [ "${DOWNLOAD_FLAIR_ES}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair EMBEDDING ES prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else 
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_ES_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING wiki-Fasttext ES prerequisites [${EMBEDDING_FASTTEXT_WIKI_ES_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_ES_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING wiki-Fasttext ES prerequisite already satisfied"
  fi
  EMBEDDING=true
  for EMBEDDING in $EMBEDDING_FWBW_LM_ES_FILES ; do
    if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
      EMBEDDING=false
    fi
  done
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING ForwardBackward ES prerequisites [${EMBEDDING_FWBW_LM_ES_DL}]"
    wget -qO- "${EMBEDDING_FWBW_LM_ES_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING ForwardBackward ES prerequisite already satisfied"
  fi
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}"/es ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"/es
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_BYTEPAIR_ES_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/es/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"/es
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING Bytepair ES prerequisites [${EMBEDDING_BYTEPAIR_ES_DL}]"
    wget -qO- "${EMBEDDING_BYTEPAIR_ES_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING Bytepair ES prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair EMBEDDING ES prerequisites [${ELAPSED}]"
fi

# fa-wiki-fasttext-300d-1M.vectors.npy
# ForwardBackward LM-FA
if [ "${DOWNLOAD_FLAIR_FA}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair EMBEDDING FA prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_FA_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING wiki-Fasttext FA prerequisites [${EMBEDDING_FASTTEXT_WIKI_FA_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_FA_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING wiki-Fasttext FA prerequisite already satisfied"
  fi
  EMBEDDING=true
  for EMBEDDING in $EMBEDDING_FWBW_LM_FA_FILES ; do
    if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
      EMBEDDING=false
    fi
  done
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING ForwardBackward FA prerequisites [${EMBEDDING_FWBW_LM_FA_DL}]"
    wget -qO- "${EMBEDDING_FWBW_LM_FA_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING ForwardBackward FA prerequisite already satisfied"
  fi
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}"/fa ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"/fa
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_BYTEPAIR_FA_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/fa/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"/fa
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING Bytepair FA prerequisites [${EMBEDDING_BYTEPAIR_FA_DL}]"
    wget -qO- "${EMBEDDING_BYTEPAIR_FA_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING Bytepair FA prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair EMBEDDING FA prerequisites [${ELAPSED}]"
fi

# fr-wiki-fasttext-300d-1M.vectors.npy
# ForwardBackward LM-FR
if [ "${DOWNLOAD_FLAIR_FR}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair EMBEDDING FR prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else 
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_FR_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING wiki-Fasttext FR prerequisites [${EMBEDDING_FASTTEXT_WIKI_FR_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_FR_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING wiki-Fasttext FR prerequisite already satisfied"
  fi
  EMBEDDING=true
  for EMBEDDING in $EMBEDDING_FWBW_LM_FR_FILES ; do
    if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
      EMBEDDING=false
    fi
  done
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING ForwardBackward FR prerequisites [${EMBEDDING_FWBW_LM_FR_DL}]"
    wget -qO- "${EMBEDDING_FWBW_LM_FR_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING ForwardBackward FR prerequisite already satisfied"
  fi
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}"/fr ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"/fr
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_BYTEPAIR_FR_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/fr/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"/fr
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING Bytepair FR prerequisites [${EMBEDDING_BYTEPAIR_FR_DL}]"
    wget -qO- "${EMBEDDING_BYTEPAIR_FR_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING Bytepair FR prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair EMBEDDING FR prerequisites [${ELAPSED}]"
fi

# it-wiki-fasttext-300d-1M.vectors.npy
# ForwardBackward LM-IT
if [ "${DOWNLOAD_FLAIR_IT}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair EMBEDDING IT prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_IT_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING wiki-Fasttext IT prerequisites [${EMBEDDING_FASTTEXT_WIKI_IT_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_IT_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING wiki-Fasttext IT prerequisite already satisfied"
  fi
  EMBEDDING=true
  for EMBEDDING in $EMBEDDING_FWBW_LM_IT_FILES ; do
    if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
      EMBEDDING=false
    fi
  done
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING ForwardBackward IT prerequisites [${EMBEDDING_FWBW_LM_IT_DL}]"
    wget -qO- "${EMBEDDING_FWBW_LM_IT_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING ForwardBackward IT prerequisite already satisfied"
  fi
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}"/it ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"/it
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_BYTEPAIR_IT_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/it/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"/it
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING Bytepair IT prerequisites [${EMBEDDING_BYTEPAIR_IT_DL}]"
    wget -qO- "${EMBEDDING_BYTEPAIR_IT_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING Bytepair IT prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair EMBEDDING IT prerequisites [${ELAPSED}]"
fi

# ja-wiki-fasttext-300d-1M.vectors.npy
# ForwardBackward LM-JA
if [ "${DOWNLOAD_FLAIR_JA}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair EMBEDDING JA prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_JA_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING wiki-Fasttext JA prerequisites [${EMBEDDING_FASTTEXT_WIKI_JA_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_JA_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING wiki-Fasttext JA prerequisite already satisfied"
  fi
  EMBEDDING=true
  for EMBEDDING in $EMBEDDING_FWBW_LM_JA_FILES ; do
    if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
      EMBEDDING=false
    fi
  done
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING ForwardBackward JA prerequisites [${EMBEDDING_FWBW_LM_JA_DL}]"
    wget -qO- "${EMBEDDING_FWBW_LM_JA_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING ForwardBackward JA prerequisite already satisfied"
  fi
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}"/ja ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"/ja
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_BYTEPAIR_JA_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/ja/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"/ja
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING Bytepair JA prerequisites [${EMBEDDING_BYTEPAIR_JA_DL}]"
    wget -qO- "${EMBEDDING_BYTEPAIR_JA_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING Bytepair JA prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair EMBEDDING JA prerequisites [${ELAPSED}]"
fi

# nl-wiki-fasttext-300d-1M.vectors.npy
# ForwardBackward LM-NL
if [ "${DOWNLOAD_FLAIR_NL}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair EMBEDDING NL prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else 
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_NL_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING wiki-Fasttext NL prerequisites [${EMBEDDING_FASTTEXT_WIKI_NL_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_NL_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING wiki-Fasttext NL prerequisite already satisfied"
  fi
  EMBEDDING=true
  for EMBEDDING in $EMBEDDING_FWBW_LM_NL_FILES ; do
    if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
      EMBEDDING=false
    fi
  done
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING ForwardBackward NL prerequisites [${EMBEDDING_FWBW_LM_NL_DL}]"
    wget -qO- "${EMBEDDING_FWBW_LM_NL_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING ForwardBackward NL prerequisite already satisfied"
  fi
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}"/nl ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"/nl
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_BYTEPAIR_NL_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/nl/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"/nl
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING Bytepair NL prerequisites [${EMBEDDING_BYTEPAIR_NL_DL}]"
    wget -qO- "${EMBEDDING_BYTEPAIR_NL_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING Bytepair NL prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair EMBEDDING NL prerequisites [${ELAPSED}]"
fi

# ar-wiki-fasttext-300d-1M.vectors.npy
# ForwardBackward LM-AR
if [ "${DOWNLOAD_FLAIR_AR}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair EMBEDDING AR prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else 
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_AR_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING wiki-Fasttext AR prerequisites [${EMBEDDING_FASTTEXT_WIKI_AR_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_AR_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING wiki-Fasttext AR prerequisite already satisfied"
  fi
  EMBEDDING=true
  for EMBEDDING in $EMBEDDING_FWBW_LM_AR_FILES ; do
    if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
      EMBEDDING=false
    fi
  done
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING ForwardBackward AR prerequisites [${EMBEDDING_FWBW_LM_AR_DL}]"
    wget -qO- "${EMBEDDING_FWBW_LM_AR_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING ForwardBackward AR prerequisite already satisfied"
  fi
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}"/ar ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"/ar
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_BYTEPAIR_AR_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/ar/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"/ar
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING Bytepair AR prerequisites [${EMBEDDING_BYTEPAIR_AR_DL}]"
    wget -qO- "${EMBEDDING_BYTEPAIR_AR_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING Bytepair AR prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair EMBEDDING AR prerequisites [${ELAPSED}]"
fi

# hi-wiki-fasttext-300d-1M.vectors.npy
if [ "${DOWNLOAD_FLAIR_HI}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair EMBEDDING HI prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else 
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_HI_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING wiki-Fasttext HI prerequisites [${EMBEDDING_FASTTEXT_WIKI_HI_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_HI_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING wiki-Fasttext HI prerequisite already satisfied"
  fi
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}"/hi ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"/hi
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_BYTEPAIR_HI_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/hi/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"/hi
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING Bytepair HI prerequisites [${EMBEDDING_BYTEPAIR_HI_DL}]"
    wget -qO- "${EMBEDDING_BYTEPAIR_HI_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING Bytepair HI prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair EMBEDDING HI prerequisites [${ELAPSED}]"
fi

# zh-wiki-fasttext-300d-1M.vectors.npy
if [ "${DOWNLOAD_FLAIR_ZH}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair EMBEDDING ZH prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else 
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_ZH_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING wiki-Fasttext ZH prerequisites [${EMBEDDING_FASTTEXT_WIKI_ZH_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_ZH_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING wiki-Fasttext ZH prerequisite already satisfied"
  fi
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}"/zh ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"/zh
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_BYTEPAIR_ZH_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/zh/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"/zh
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING Bytepair ZH prerequisites [${EMBEDDING_BYTEPAIR_ZH_DL}]"
    wget -qO- "${EMBEDDING_BYTEPAIR_ZH_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING Bytepair ZH prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair EMBEDDING ZH prerequisites [${ELAPSED}]"
fi

# ru-wiki-fasttext-300d-1M.vectors.npy
if [ "${DOWNLOAD_FLAIR_RU}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair EMBEDDING RU prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_RU_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING wiki-Fasttext RU prerequisites [${EMBEDDING_FASTTEXT_WIKI_RU_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_RU_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING wiki-Fasttext RU prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair EMBEDDING RU prerequisites [${ELAPSED}]"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}"/ru ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"/ru
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_BYTEPAIR_RU_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/ru/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"/ru
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING Bytepair RU prerequisites [${EMBEDDING_BYTEPAIR_RU_DL}]"
    wget -qO- "${EMBEDDING_BYTEPAIR_RU_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING Bytepair RU prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair EMBEDDING RU prerequisites [${ELAPSED}]"
fi

# pt-wiki-fasttext-300d-1M.vectors.npy
if [ "${DOWNLOAD_FLAIR_PT}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair EMBEDDING PT prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FASTTEXT_WIKI_PT_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING wiki-Fasttext PT prerequisites [${EMBEDDING_FASTTEXT_WIKI_PT_DL}]"
    wget -qO- "${EMBEDDING_FASTTEXT_WIKI_PT_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING wiki-Fasttext PT prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair EMBEDDING PT prerequisites [${ELAPSED}]"
  EMBEDDING=true
  for EMBEDDING in $EMBEDDING_FWBW_LM_PT_FILES ; do
    if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
      EMBEDDING=false
    fi
  done
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING ForwardBackward PT prerequisites [${EMBEDDING_FWBW_LM_PT_DL}]"
    wget -qO- "${EMBEDDING_FWBW_LM_PT_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING ForwardBackward PT prerequisite already satisfied"
  fi
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}"/pt ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"/pt
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_BYTEPAIR_PT_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/pt/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"/pt
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING Bytepair PT prerequisites [${EMBEDDING_BYTEPAIR_PT_DL}]"
    wget -qO- "${EMBEDDING_BYTEPAIR_PT_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING Bytepair PT prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair EMBEDDING PT prerequisites [${ELAPSED}]"
fi

# ForwardBackward LM-XX
if [ "${DOWNLOAD_FLAIR_XX}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair EMBEDDING XX prerequisites"
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_FWBW_LM_XX_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING ForwardBackward XX prerequisites [${EMBEDDING_FWBW_LM_XX_DL}]"
    wget -qO- "${EMBEDDING_FWBW_LM_XX_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING ForwardBackward XX prerequisite already satisfied"
  fi
  EMBEDDING=true
  if [ ! -d "${PATH_EMBEDDINGS}"/multi ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"/multi
    EMBEDDING=false
  else
    for EMBEDDING in $EMBEDDING_BYTEPAIR_XX_FILES ; do
      if [ ! -f "${PATH_EMBEDDINGS}"/multi/"${EMBEDDING}" ] ; then
        EMBEDDING=false
      fi
    done
  fi
  if [ "${EMBEDDING}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"/multi
    echo " --[$(date)]: |-Downloading and extracting Flair EMBEDDING Bytepair XX prerequisites [${EMBEDDING_BYTEPAIR_XX_DL}]"
    wget -qO- "${EMBEDDING_BYTEPAIR_XX_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair EMBEDDING Bytepair XX prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair EMBEDDING XX prerequisites [${ELAPSED}]"
fi
#-------- EMBEDDING --------

#-------- LMDB --------
# de-wiki-fasttext-300d-1M.lmdb
if [ "${DOWNLOAD_FLAIR_DE}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair LMDB DE prerequisites"
  LMDB=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    LMDB=false
  else
    for LMDB in $LMDB_WIKI_DE_FOLDER ; do
      if [ ! -d "${PATH_EMBEDDINGS}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_EMBEDDINGS}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_EMBEDDINGS}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair prerequisites [${LMDB_WIKI_DE_DL}]"
    wget -qO- "${LMDB_WIKI_DE_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair LMDB DE prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair LMDB DE prerequisites [${ELAPSED}]"
fi

# en-fasttext-news-300d-1M.lmdb
if [ "${DOWNLOAD_FLAIR_EN}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair LMDB EN prerequisites"
  LMDB=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    LMDB=false
  else
    for LMDB in $LMDB_NEWS_EN_FOLDER ; do
      if [ ! -d "${PATH_EMBEDDINGS}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_EMBEDDINGS}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_EMBEDDINGS}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair prerequisites [${LMDB_NEWS_EN_DL}]"
    wget -qO- "${LMDB_NEWS_EN_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair LMDB EN prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair LMDB EN prerequisites [${ELAPSED}]"
fi

# es-wiki-fasttext-300d-1M.lmdb
if [ "${DOWNLOAD_FLAIR_ES}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair LMDB ES prerequisites"
  LMDB=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    LMDB=false
  else
    for LMDB in $LMDB_WIKI_ES_FOLDER ; do
      if [ ! -d "${PATH_EMBEDDINGS}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_EMBEDDINGS}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_EMBEDDINGS}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair prerequisites [${LMDB_WIKI_ES_DL}]"
    wget -qO- "${LMDB_WIKI_ES_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair LMDB ES prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair LMDB ES prerequisites [${ELAPSED}]"
fi

# fa-wiki-fasttext-300d-1M.lmdb
if [ "${DOWNLOAD_FLAIR_FA}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair LMDB FA prerequisites"
  LMDB=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    LMDB=false
  else
    for LMDB in $LMDB_WIKI_FA_FOLDER ; do
      if [ ! -d "${PATH_EMBEDDINGS}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_EMBEDDINGS}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_EMBEDDINGS}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair prerequisites [${LMDB_WIKI_FA_DL}]"
    wget -qO- "${LMDB_WIKI_FA_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair LMDB FA prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair LMDB FA prerequisites [${ELAPSED}]"
fi

# fr-wiki-fasttext-300d-1M.lmdb
if [ "${DOWNLOAD_FLAIR_FR}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair LMDB FR prerequisites"
  LMDB=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    LMDB=false
  else
    for LMDB in $LMDB_WIKI_FR_FOLDER ; do
      if [ ! -d "${PATH_EMBEDDINGS}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_EMBEDDINGS}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_EMBEDDINGS}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair prerequisites [${LMDB_WIKI_FR_DL}]"
    wget -qO- "${LMDB_WIKI_FR_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair LMDB FR prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair LMDB FR prerequisites [${ELAPSED}]"
fi

# it-wiki-fasttext-300d-1M.lmdb
if [ "${DOWNLOAD_FLAIR_IT}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair LMDB IT prerequisites"
  LMDB=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    LMDB=false
  else 
    for LMDB in $LMDB_WIKI_IT_FOLDER ; do
      if [ ! -d "${PATH_EMBEDDINGS}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_EMBEDDINGS}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_EMBEDDINGS}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair prerequisites [${LMDB_WIKI_IT_DL}]"
    wget -qO- "${LMDB_WIKI_IT_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair LMDB IT prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair LMDB IT prerequisites [${ELAPSED}]"
fi

# ja-wiki-fasttext-300d-1M.lmdb
if [ "${DOWNLOAD_FLAIR_JA}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair LMDB JA prerequisites"
  LMDB=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    LMDB=false
  else
    for LMDB in $LMDB_WIKI_JA_FOLDER ; do
      if [ ! -d "${PATH_EMBEDDINGS}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_EMBEDDINGS}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_EMBEDDINGS}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair prerequisites [${LMDB_WIKI_JA_DL}]"
    wget -qO- "${LMDB_WIKI_JA_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair LMDB JA prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair LMDB JA prerequisites [${ELAPSED}]"
fi

# nl-wiki-fasttext-300d-1M.lmdb
if [ "${DOWNLOAD_FLAIR_NL}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair LMDB NL prerequisites"
  LMDB=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    LMDB=false
  else
    for LMDB in $LMDB_WIKI_NL_FOLDER ; do
      if [ ! -d "${PATH_EMBEDDINGS}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_EMBEDDINGS}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_EMBEDDINGS}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair prerequisites [${LMDB_WIKI_NL_DL}]"
    wget -qO- "${LMDB_WIKI_NL_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair LMDB NL prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair LMDB NL prerequisites [${ELAPSED}]"
fi

# ar-wiki-fasttext-300d-1M.lmdb
if [ "${DOWNLOAD_FLAIR_AR}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair LMDB AR prerequisites"
  LMDB=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    LMDB=false
  else 
    for LMDB in $LMDB_WIKI_AR_FOLDER ; do
      if [ ! -d "${PATH_EMBEDDINGS}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_EMBEDDINGS}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_EMBEDDINGS}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair prerequisites [${LMDB_WIKI_AR_DL}]"
    wget -qO- "${LMDB_WIKI_AR_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair LMDB AR prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair LMDB AR prerequisites [${ELAPSED}]"
fi

# hi-wiki-fasttext-300d-1M.lmdb
if [ "${DOWNLOAD_FLAIR_HI}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair LMDB HI prerequisites"
  LMDB=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    LMDB=false
  else
    for LMDB in $LMDB_WIKI_HI_FOLDER ; do
      if [ ! -d "${PATH_EMBEDDINGS}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_EMBEDDINGS}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_EMBEDDINGS}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair prerequisites [${LMDB_WIKI_HI_DL}]"
    wget -qO- "${LMDB_WIKI_HI_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair LMDB HI prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair LMDB HI prerequisites [${ELAPSED}]"
fi

# zh-wiki-fasttext-300d-1M.lmdb
if [ "${DOWNLOAD_FLAIR_ZH}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair LMDB ZH prerequisites"
  LMDB=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    LMDB=false
  else 
    for LMDB in $LMDB_WIKI_ZH_FOLDER ; do
      if [ ! -d "${PATH_EMBEDDINGS}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_EMBEDDINGS}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_EMBEDDINGS}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair prerequisites [${LMDB_WIKI_ZH_DL}]"
    wget -qO- "${LMDB_WIKI_ZH_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair LMDB ZH prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair LMDB ZH prerequisites [${ELAPSED}]"
fi

# ru-wiki-fasttext-300d-1M.lmdb
if [ "${DOWNLOAD_FLAIR_RU}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair LMDB RU prerequisites"
  LMDB=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    LMDB=false
  else
    for LMDB in $LMDB_WIKI_RU_FOLDER ; do
      if [ ! -d "${PATH_EMBEDDINGS}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_EMBEDDINGS}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_EMBEDDINGS}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair prerequisites [${LMDB_WIKI_RU_DL}]"
    wget -qO- "${LMDB_WIKI_RU_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair LMDB RU prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair LMDB RU prerequisites [${ELAPSED}]"
fi

# pt-wiki-fasttext-300d-1M.lmdb
if [ "${DOWNLOAD_FLAIR_PT}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair LMDB PT prerequisites"
  LMDB=true
  if [ ! -d "${PATH_EMBEDDINGS}" ] ; then
    mkdir -p "${PATH_EMBEDDINGS}"
    LMDB=false
  else
    for LMDB in $LMDB_WIKI_PT_FOLDER ; do
      if [ ! -d "${PATH_EMBEDDINGS}"/"${LMDB}" ] ; then
        LMDB=false
      elif [ ! -n "$( ls -A "${PATH_EMBEDDINGS}"/"${LMDB}" )" ] ; then
        #rm -rf "${PATH_EMBEDDINGS}"/"${LMDB}"
        LMDB=false
      fi
    done
  fi
  if [ "${LMDB}" == "false" ] ; then
    cd "${PATH_EMBEDDINGS}"
    echo " --[$(date)]: |-Downloading and extracting Flair prerequisites [${LMDB_WIKI_PT_DL}]"
    wget -qO- "${LMDB_WIKI_PT_DL}" | bsdtar -xf-
  else
    echo " --[$(date)]: |-Flair LMDB PT prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair LMDB PT prerequisites [${ELAPSED}]"
fi
#-------- LMDB --------

#-------- DATASETS --------
# common_characters
DATASETS=true
if [ ! -d "$PATH_DATASET" ] ; then
  mkdir -p "$PATH_DATASET"
  DATASETS=false
else 
  for DATASET in $DATASET_COMMON_CHARACTERS ; do
    if [ ! -f "${PATH_DATASET}"/"${DATASET}" ] ; then
      DATASETS=false
    fi
  done
fi
if [ "${DATASETS}" == "false" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of Flair DATASETS prerequisites"
  cd "$PATH_DATASET"
  echo " --[$(date)]: |-Downloading and extracting Flair prerequisites [${DATASET_COMMON_CHARACTERS_DL}]"
  wget -qO- "${DATASET_COMMON_CHARACTERS_DL}" | bsdtar -xf-
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of Flair DATASETS prerequisites [${ELAPSED}]"
else
  echo " --[$(date)]: |-Flair DATASETS prerequisite already satisfied"
fi
#-------- DATASETS --------

# Give permissions to workdir
chown -R kairntech:kairntech "${PATH_EMBEDDINGS}" "${PATH_DATASET}"

GDATE_END=$( date +%s )
DIFF_SECONDS=$(( GDATE_END - GDATE_START ))
GELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
echo "[$(date)]: Ending download of Flair prerequisites [${GELAPSED}]"
