#!/bin/bash

# To be inserted in crontab :
#Docker start sherpa
#@reboot /usr/bin/dockermanager.sh -S -u 1>/tmp/_startdocker.out 2>/tmp/_startdocker.err

# Script arguments analysis
for i in "$@"
do
        case $i in
        -h|--help)
                HELP=true
                ;;
        -S|--sherpa)
                SHERPA=true
                ;;
        -E|--entity-fishing)
                ENTITYFISHING=true
                ;;
        -G|--grobid)
                GROBID=true
                ;;
        -c|--cleanlogs)
                CLEANLOGS=true
                ;;
        -p|--pull)
                PULL=true
                ;;
        -u|--up)
                UP=true
                ;;
        -d|--down)
                DOWN=true
                ;;
        -r|--restart)
                RESTART=true
                ;;
        -w|--whatismyip)
                WHATISMYIP=true
                ;;
        -pi|--prune-images)
                PRUNEIMAGES=true
                ;;
        -pv|--prune-volumes)
                PRUNEVOLUMES=true
                ;;
        -z|--raz-counter)
                RAZCOUNTERS=true
                ;;
        *)
                # unknown option
                HELP=true
                ;;
        esac
done

# Help printing
if [ "$HELP" == "true" ] || [ -z "$1" ] ; then
  echo "$0 - help and usage"
  echo "      -h |--help            : print this help and exit"
  echo "      -w |--whatismyip      : give external IP of server"
  echo
  echo "      -S |--sherpa          : command(s) are executed on Sherpa platform"
  echo "      -E |--entity-fishing  : command(s) are executed on Entity-Fishing platform"
  echo "      -G |--grobid          : command(s) are executed on Grobid platform"
  echo "        -c |--cleanlogs     : clean docker log volumes"
  echo "        -u |--up            : start docker containers (in detached mode)"
  echo "        -d |--down          : stop docker containers"
  echo "        -r |--restart       : restart docker containers (in detached mode)"
  echo "        -p |--pull          : pull docker images (stop containers before, pull, then start containers)"
  echo "        -pi|--prune-images  : prune docker images"
  echo "        -pv|--prune-volumes : prune docker volumes"
  echo "        -z |--raz-counter   : reset sherpa counters (oom, drops)"
  exit 0
fi

# Ensure DNS are not KO
HTTP1=$( curl -s -o /dev/null -w "%{http_code}" whatismyip.akamai.com )
HTTP2=$( curl -s -o /dev/null -w "%{http_code}" ipinfo.io/ip )
if [ "${HTTP1}" -ne 200 ] || [ "${HTTP2}" -ne 200 ] ; then
  echo "It seems that there is a major DNS issue with whatismyip.akamai.com/ipinfo.io, exiting.."
  exit 1
fi

# Get IP address
IPADDRESS=
CNT=0
while [ $CNT -lt 10 ] ; do
  IP0=$( curl -s -4 whatismyip.akamai.com )
  if [ "$IP0" == "" ] ; then
    echo "It seems that your server is not giving back IP (from whatismyip.akamai.com), sleeping 1s.."
    sleep 1
    CNT=$(( CNT + 1 ))
  else
    CNT=10
  fi
done
IP1=$( curl -s -4 whatismyip.akamai.com )
IP2=$( curl -s -4 ipinfo.io/ip )
if [ "$IP1" == "$IP2" ] ; then
  IPADDRESS=$IP1
fi
if [ "$IPADDRESS" == "" ] ; then
  echo "It seems that your IP address is giving mismatch, exiting.."
  echo " - IP [$IP1] is returned by 'whatismyip.akamai.com'"
  echo " - IP [$IP2] is returned by 'ipinfo.io/ip'"
  exit 1
fi
if [ "$WHATISMYIP" == "true" ] ; then
  echo "Your IP address is $IP1"
  exit 0
fi

# Get User
USER=
EX_USER=$( whoami )
USERS=$( grep docker /etc/group|cut -d ':' -f4|tr ',' '\n' )
for L_USER in $USERS ; do
  if [ "${L_USER}" == "${EX_USER}" ] ; then
    USER=$L_USER
  fi
done
if [ "$USER" == "" ] ; then
  echo "It seems that your user is not associated to docker group, exiting.."
  exit 1
fi

# Start Operations
echo "*** $(date) *** Starting ***"

# Identify if sherpa (studio) is running
SHERPAS=$( docker ps | grep -v sherpaprod-core | grep -c sherpa-core )
# Identify if docker-compose.yml is deployed
if [ "${SHERPAS}" -ne 1 ] ; then
  if [ -d /opt/sherpa ] ; then
    SHERPAS=$( find /opt/sherpa -type f -name docker-compose.yml -printf x | wc -c )
  fi
fi
# Identify if sherpa (prod) is running
SHERPAP=$( docker ps | grep -v sherpa-core | grep -c sherpaprod-core )
# Identify if docker-compose.prod.yml is deployed
if [ "${SHERPAP}" -ne 1 ] ; then
  if [ -d /opt/sherpa ] ; then
    SHERPAP=$( find /opt/sherpa -type f -name docker-compose.prod.yml -printf x | wc -c )
  fi
fi

# BEGIN SHERPA LOOP
if [ "$SHERPA" == "true" ] ; then

  # Stop Sherpa
  if [ "$PULL" == "true" ] || [ "$RESTART" == "true" ] || [ "$DOWN" == "true" ] ; then
    if [ "${SHERPAS}" -eq 1 ] ; then
      echo "*** $(date) **** sherpa ** Stopping docker containers ***"
      cd /opt/sherpa && docker compose -p sherpa -f docker-compose.yml down 1>/tmp/_sherpa.out 2>/tmp/_sherpa.err
    fi
    if [ "${SHERPAP}" -eq 1 ] ; then
      echo "*** $(date) **** sherpaprod ** Stopping docker containers ***"
      cd /opt/sherpa && docker compose -p sherpa-prod -f docker-compose.prod.yml down 1>/tmp/_sherpa.prod.out 2>/tmp/_sherpa.prod.err
    fi
  fi

  # Clean log volumes
  if [ "$CLEANLOGS" == "true" ] ; then
    if [ "${SHERPAS}" -eq 1 ] ; then
      echo "*** $(date) **** sherpa ** Cleaning docker log volumes ***"
      docker volume rm sherpa_logs-sherpa \
                       sherpa_logs-elasticsearch \
                       sherpa_logs-kibana \
                       sherpa_logs-mongodb \
                       sherpa_logs-crfsuite-suggester \
                       sherpa_logs-entityfishing-suggester \
                       sherpa_logs-phrasematcher-test-suggester \
                       sherpa_logs-phrasematcher-train-suggester \
                       sherpa_logs-entityruler-test-suggester \
                       sherpa_logs-entityruler-train-suggester \
                       sherpa_logs-delft-test-suggester \
                       sherpa_logs-delft-train-suggester \
                       sherpa_logs-spacy-test-suggester \
                       sherpa_logs-spacy-train-suggester \
                       sherpa_logs-sklearn-test-suggester \
                       sherpa_logs-sklearn-train-suggester \
                       sherpa_logs-flair-test-suggester \
                       sherpa_logs-flair-train-suggester \
                       sherpa_logs-builtins-importer \
                       sherpa_logs-multirole \
                       sherpa_logs-pymultirole \
                       sherpa_logs-pymultirole-ner \
                       sherpa_logs-pymultirole-trf \
                       sherpa_sherpa-lock \
                       sherpa_prometheus \
                       > /dev/null 2>&1
    fi
    if [ "${SHERPAP}" -eq 1 ] ; then
      echo "*** $(date) **** sherpaprod ** Cleaning docker log volumes ***"
      docker volume rm sherpaprod_logs-sherpa \
                       sherpaprod_logs-crfsuite-suggester \
                       sherpaprod_logs-entityfishing-suggester \
                       sherpaprod_logs-phrasematcher-test-suggester \
                       sherpaprod_logs-entityruler-test-suggester \
                       sherpaprod_logs-delft-test-suggester \
                       sherpaprod_logs-sklearn-test-suggester \
                       sherpaprod_logs-spacy-test-suggester \
                       sherpaprod_logs-multirole \
                       sherpaprod_logs-pymultirole \
                       sherpaprod_sherpa-lock \
                       > /dev/null 2>&1
    fi
  fi

  # Update Sherpa
  if [ "$PULL" == "true" ] ; then
    if [ "${SHERPAS}" -eq 1 ] ; then
      echo "*** $(date) **** sherpa ** Updating docker images ***"
      cd /opt/sherpa && docker compose -p sherpa -f docker-compose.yml pull --ignore-pull-failures 1>>/tmp/_sherpa.out 2>>/tmp/_sherpa.err
    fi
    if [ "${SHERPAP}" -eq 1 ] ; then
      echo "*** $(date) **** sherpaprod ** Updating docker images ***"
      cd /opt/sherpa && docker compose -p sherpa-prod -f docker-compose.prod.yml pull --ignore-pull-failures 1>>/tmp/_sherpa.prod.out 2>>/tmp/_sherpa.prod.err
    fi
  fi

  # Start Sherpa
  if [ "$PULL" == "true" ] || [ "$RESTART" == "true" ] || [ "$UP" == "true" ] ; then
    if [ "${SHERPAS}" -eq 1 ] ; then
      echo "*** $(date) **** sherpa ** Starting docker containers ***"
      cd /opt/sherpa && docker compose -p sherpa -f docker-compose.yml up -d 1>>/tmp/_sherpa.out 2>>/tmp/_sherpa.err
    fi
    if [ "${SHERPAP}" -eq 1 ] ; then
      echo "*** $(date) **** sherpaprod ** Starting docker containers ***"
      cd /opt/sherpa && docker compose -p sherpa-prod -f docker-compose.prod.yml up -d 1>>/tmp/_sherpa.prod.out 2>>/tmp/_sherpa.prod.err
    fi
  fi

  # Prune old images
  if [ "$PRUNEIMAGES" == "true" ] ; then
    echo "*** $(date) **** sherpa ** Pruning unused docker images ***"
    yes|docker image prune -a > /dev/null 2>&1
  fi

  # Prune old volumes
  if [ "$PRUNEVOLUMES" == "true" ] ; then
    echo "*** $(date) **** sherpa ** Pruning unused docker volumes ***"
    yes|docker volume prune --filter all=1 > /dev/null 2>&1
  fi

  # Reset crontab counters
  if [ "$RAZCOUNTERS" == "true" ] ; then
    FOLDER=
    if [ -d "/opt/monitor-sherpa" ] ; then
      FOLDER=/opt/monitor-sherpa
    elif [ -d "/mnt/sherpa-data/sherpa-sniffer" ] ; then
      FOLDER=/mnt/sherpa-data/sherpa-sniffer
    elif [ -d "/mnt/docker/sherpa-sniffer" ] ; then
      FOLDER=/mnt/docker/sherpa-sniffer
    elif [ -d "/home/${USER}/dev/kairntech/sherpa-sniffer" ] ; then
      FOLDER=/home/${USER}/dev/kairntech/sherpa-sniffer
    fi
    if [ "${FOLDER}" != "" ] ; then
      echo "*** $(date) **** sherpa ** Reseting crontab counters in ${FOLDER} ***"
      echo "0" > "${FOLDER}"/.nbdrops
      echo "0" > "${FOLDER}"/.nbooms-sherpa
      echo "0" > "${FOLDER}"/.nbooms-crf
      echo "0" > "${FOLDER}"/.nbooms-ef
      echo "0" > "${FOLDER}"/.nbooms-fasttext
      echo "0" > "${FOLDER}"/.nbooms-pm
      echo "0" > "${FOLDER}"/.nbooms-delft
      echo "0" > "${FOLDER}"/.nbooms-spacy
      echo "0" > "${FOLDER}"/.nbooms-flair
      echo "0" > "${FOLDER}"/.nbooms-sklearn
      echo "0" > "${FOLDER}"/.nbooms-bertopic
      echo "0" > "${FOLDER}"/.nbooms-trankit
      echo "0" > "${FOLDER}"/.nbooms-trf
    fi
  fi
fi
# END SHERPA LOOP

# BEGIN ENTITYFISHING LOOP
if [ "$ENTITYFISHING" == "true" ] ; then
  # Stop Entity-Fishing
  if [ "$PULL" == "true" ] || [ "$RESTART" == "true" ] || [ "$DOWN" == "true" ] ; then
    echo "*** $(date) **** entity-fishing ** Stopping docker containers ***"
    cd /opt/entity-fishing && docker compose -f docker-compose.yml down 1>/tmp/_entityf.out 2>/tmp/_entityf.err
  fi

  # Update Entity-Fishing
  if [ "$PULL" == "true" ] ; then
    echo "*** $(date) **** entity-fishing ** Updating docker images ***"
    cd /opt/entity-fishing && docker compose -f docker-compose.yml pull --ignore-pull-failures 1>/tmp/_entityf.out 2>/tmp/_entityf.err
  fi

  # Start Entity-Fishing
  if [ "$PULL" == "true" ] || [ "$RESTART" == "true" ] || [ "$UP" == "true" ] ; then
    echo "*** $(date) **** entity-fishing ** Starting docker containers ***"
    cd /opt/entity-fishing && docker compose -f docker-compose.yml up -d 1>>/tmp/_entityf.out 2>>/tmp/_entityf.err
  fi

  # Inject custom files
  if [ "$PULL" == "true" ] || [ "$RESTART" == "true" ] || [ "$UP" == "true" ] ; then
    if [ -d "/opt/entity-fishing/webapp" ] ; then
      echo "*** $(date) **** entity-fishing ** WarmUp: Injecting custom files ***"
      cd /opt/entity-fishing && \
      docker cp webapp/favicon.ico sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/img/favicon.ico && \
      docker cp webapp/index.html sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/index.html && \
      docker cp webapp/nerd.js sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/nerd/nerd.js && \
      docker cp webapp/query-examples/kairntech-ar1.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-ar1.json && \
      docker cp webapp/query-examples/kairntech-ar2.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-ar2.json && \
      docker cp webapp/query-examples/kairntech-ar3.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-ar3.json && \
      docker cp webapp/query-examples/kairntech-de1.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-de1.json && \
      docker cp webapp/query-examples/kairntech-de2.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-de2.json && \
      docker cp webapp/query-examples/kairntech-de3.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-de3.json && \
      docker cp webapp/query-examples/kairntech-en1.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-en1.json && \
      docker cp webapp/query-examples/kairntech-en2.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-en2.json && \
      docker cp webapp/query-examples/kairntech-en3.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-en3.json && \
      docker cp webapp/query-examples/kairntech-es1.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-es1.json && \
      docker cp webapp/query-examples/kairntech-es2.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-es2.json && \
      docker cp webapp/query-examples/kairntech-es3.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-es3.json && \
      docker cp webapp/query-examples/kairntech-fa1.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-fa1.json && \
      docker cp webapp/query-examples/kairntech-fa2.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-fa2.json && \
      docker cp webapp/query-examples/kairntech-fa3.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-fa3.json && \
      docker cp webapp/query-examples/kairntech-fr1.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-fr1.json && \
      docker cp webapp/query-examples/kairntech-fr2.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-fr2.json && \
      docker cp webapp/query-examples/kairntech-fr3.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-fr3.json && \
      docker cp webapp/query-examples/kairntech-it1.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-it1.json && \
      docker cp webapp/query-examples/kairntech-it2.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-it2.json && \
      docker cp webapp/query-examples/kairntech-it3.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-it3.json && \
      docker cp webapp/query-examples/kairntech-ja1.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-ja1.json && \
      docker cp webapp/query-examples/kairntech-ja2.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-ja2.json && \
      docker cp webapp/query-examples/kairntech-ja3.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-ja3.json && \
      docker cp webapp/query-examples/kairntech-pt1.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-pt1.json && \
      docker cp webapp/query-examples/kairntech-pt2.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-pt2.json && \
      docker cp webapp/query-examples/kairntech-pt3.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-pt3.json && \
      docker cp webapp/query-examples/kairntech-ru1.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-ru1.json && \
      docker cp webapp/query-examples/kairntech-ru2.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-ru2.json && \
      docker cp webapp/query-examples/kairntech-ru3.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-ru3.json && \
      docker cp webapp/query-examples/kairntech-bd1.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-bd1.json && \
      docker cp webapp/query-examples/kairntech-bd2.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-bd2.json && \
      docker cp webapp/query-examples/kairntech-bd3.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-bd3.json && \
      docker cp webapp/query-examples/kairntech-in1.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-in1.json && \
      docker cp webapp/query-examples/kairntech-in2.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-in2.json && \
      docker cp webapp/query-examples/kairntech-in3.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-in3.json && \
      docker cp webapp/query-examples/kairntech-se1.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-se1.json && \
      docker cp webapp/query-examples/kairntech-se2.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-se2.json && \
      docker cp webapp/query-examples/kairntech-se3.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-se3.json && \
      docker cp webapp/query-examples/kairntech-ua1.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-ua1.json && \
      docker cp webapp/query-examples/kairntech-ua2.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-ua2.json && \
      docker cp webapp/query-examples/kairntech-ua3.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-ua3.json && \
      docker cp webapp/query-examples/kairntech-zh1.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-zh1.json && \
      docker cp webapp/query-examples/kairntech-zh2.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-zh2.json && \
      docker cp webapp/query-examples/kairntech-zh3.json sherpa-entityfishing:/app/kairntech/nerd/src/main/resources/web/resources/query-examples/kairntech-zh3.json
      docker exec -i sherpa-entityfishing sed -i 's/build\//build_\//g' /app/kairntech/nerd/src/main/resources/web/resources/pdf.js/index.html

      # WarmUp Entity-Fishing
      echo "*** $(date) **** entity-fishing ** WarmUp: Calling Entity-Fishing disambiguate API ***"
      WARMUP=/opt/entity-fishing/warm-up
      rm -f ${WARMUP}/*.out ${WARMUP}/*.err

      FILE=_ef.req1
      CURLOK=1
      echo "*** $(date) **** entity-fishing ** WarmUp: Entity-Fishing warming up - req1 (${FILE}.in) ***"
      while [ ${CURLOK} -ne 0 ] ; do
        curl --retry 5 --max-time 150 --retry-delay 0 --retry-max-time 60 --retry-connrefused \
         -X POST http://localhost:8090/service/disambiguate -H "Content-Type: application/json" -d @${WARMUP}/${FILE}.in \
         1>"${WARMUP}/${FILE}.out" 2>"${WARMUP}/${FILE}.err"
        #CURLOK=`cat ${WARMUP}/${FILE}.err 2>/dev/null | grep -c 'reset\|Empty reply'`
        CURLOK=$( grep -c 'reset\|Empty reply' "${WARMUP}/${FILE}.err" 2>/dev/null )
      done

      FILE=_vn.req2
      CURLOK=1
      echo "*** $(date) **** entity-fishing ** WarmUp: Entity-Fishing warming up - req2 (${FILE}.in) ***"
      while [ ${CURLOK} -ne 0 ] ; do
        curl --retry 5 --max-time 150 --retry-delay 0 --retry-max-time 60 --retry-connrefused \
         -X POST http://localhost:8090/service/disambiguate -H "Content-Type: application/json" -d @${WARMUP}/${FILE}.in \
         1>"${WARMUP}/${FILE}.out" 2>"${WARMUP}/${FILE}.err"
        #CURLOK=`cat ${WARMUP}/${FILE}.err 2>/dev/null | grep -c 'reset\|Empty reply'`
        CURLOK=$( grep -c 'reset\|Empty reply' "${WARMUP}/${FILE}.err" 2>/dev/null )
      done

      FILE=_vn.req1
      CURLOK=1
      echo "*** $(date) **** entity-fishing ** WarmUp: Entity-Fishing warming up - req3 (${FILE}.in) ***"
      while [ ${CURLOK} -ne 0 ] ; do
        curl --retry 5 --max-time 150 --retry-delay 0 --retry-max-time 60 --retry-connrefused \
         -X POST http://localhost:8090/service/disambiguate -H "Content-Type: application/json" -d @${WARMUP}/${FILE}.in \
         1>"${WARMUP}/${FILE}.out" 2>"${WARMUP}/${FILE}.err"
        #CURLOK=`cat ${WARMUP}/${FILE}.err 2>/dev/null | grep -c 'reset\|Empty reply'`
        CURLOK=$( grep -c 'reset\|Empty reply' "${WARMUP}/${FILE}.err" 2>/dev/null )
      done
    fi
  fi
fi
# END ENTITYFISHING LOOP

# BEGIN GROBID LOOP
if [ "$GROBID" == "true" ] ; then
  # Stop Grobid
  if [ "$PULL" == "true" ] || [ "$RESTART" == "true" ] || [ "$DOWN" == "true" ] ; then
    echo "*** $(date) **** grobid ** Stopping docker containers ***"
    cd /opt/grobid && docker compose -f docker-compose.yml down 1>/tmp/_grobid.out 2>/tmp/_grobid.err
  fi

  # Update Grobid
  if [ "$PULL" == "true" ] ; then
    echo "*** $(date) **** grobid ** Updating docker images ***"
    cd /opt/grobid && docker compose -f docker-compose.yml pull --ignore-pull-failures 1>/tmp/_grobid.out 2>/tmp/_grobid.err
  fi

  # Start Grobid
  if [ "$PULL" == "true" ] || [ "$RESTART" == "true" ] || [ "$UP" == "true" ] ; then
    echo "*** $(date) **** grobid ** Starting docker containers ***"
    cd /opt/grobid && docker compose -f docker-compose.yml up -d 1>>/tmp/_grobid.out 2>>/tmp/_grobid.err
  fi
fi
# END GROBID LOOP

# Ending Operations
echo "*** $(date) *** Ending ***"
