#!/bin/bash

################################################################
DOWNLOAD_EF_AR=false
DOWNLOAD_EF_BD=false
DOWNLOAD_EF_DE=false
DOWNLOAD_EF_EN=false
DOWNLOAD_EF_ES=false
DOWNLOAD_EF_FA=false
DOWNLOAD_EF_FR=false
DOWNLOAD_EF_IN=false
DOWNLOAD_EF_IT=false
DOWNLOAD_EF_JA=false
DOWNLOAD_EF_PT=false
DOWNLOAD_EF_RU=false
DOWNLOAD_EF_SE=false
DOWNLOAD_EF_UA=false
DOWNLOAD_EF_ZH=false
################################################################
PATH_EF_DATA=/data
EF_DATE="02-03-2023"
################################################################

################################################################
KNOWLEDGE_AR_FOLDER="db-ar"
KNOWLEDGE_BD_FOLDER="db-bn"
KNOWLEDGE_DE_FOLDER="db-de"
KNOWLEDGE_EN_FOLDER="db-en"
KNOWLEDGE_ES_FOLDER="db-es"
KNOWLEDGE_FA_FOLDER="db-fa"
KNOWLEDGE_FR_FOLDER="db-fr"
KNOWLEDGE_IN_FOLDER="db-hi"
KNOWLEDGE_IT_FOLDER="db-it"
KNOWLEDGE_JA_FOLDER="db-ja"
KNOWLEDGE_PT_FOLDER="db-pt"
KNOWLEDGE_RU_FOLDER="db-ru"
KNOWLEDGE_SE_FOLDER="db-sv"
KNOWLEDGE_UA_FOLDER="db-uk"
KNOWLEDGE_ZH_FOLDER="db-zh"
KNOWLEDGE_KB_FOLDER="db-kb"
################################################################

################################################################
# Catch environment
if [ -z "$1" ] ; then
  echo " - You need to add parameters such as $0 en to deploy entity-fishing EN prerequisites, exiting.."
  exit 0
elif [ "$1" == "h" ] || [ "$1" == "-h" ] || [ "$1" == "help" ] || [ "$1" == "--help" ] ; then
  echo " - Usage of $0 is:"
  echo " ---  $0 en                                 --- to deploy entity-fishing EN prerequisites"
  echo " ---  $0 de,en                              --- to deploy entity-fishing DE + EN prerequisites"
  echo " ---  $0 all                                --- to deploy all entity-fishing prerequisites"
  echo " ---  $0 all 12-03-2021                     --- to deploy all entity-fishing prerequisites, dated 3rd December, 2021"
  echo " ---  $0 http://localhost all 12-03-2021    --- to deploy all entity-fishing prerequisites, from http://localhost, dated 3rd December, 2021"
  exit 0
fi

# Manage parameters
BASE_URL="https://kairntech.s3.eu-west-3.amazonaws.com"
if [[ $1 == *"http"* ]] ; then
  BASE_URL=$1
  if [ -z "$2" ] || [ "$2" == "" ] ; then
    _LANGS="all"
  else
    _LANGS=$2
  fi
  if [ -z "$3" ] || [ "$3" == "" ] ; then
    _DATE=${EF_DATE}
  else
    _DATE=$3
  fi
else
  if [ -z "$1" ] || [ "$1" == "" ] ; then
    _LANGS="all"
  else
    _LANGS=$1
  fi
  if [ -z "$2" ] || [ "$2" == "" ] ; then
    _DATE=${EF_DATE}
  else
    _DATE=$2
  fi
fi

################################################################
# Adapt URLs
KNOWLEDGE_AR_DL="${BASE_URL}/entity-fishing/${_DATE}/db-ar.zip"
KNOWLEDGE_BD_DL="${BASE_URL}/entity-fishing/${_DATE}/db-bn.zip"
KNOWLEDGE_DE_DL="${BASE_URL}/entity-fishing/${_DATE}/db-de.zip"
KNOWLEDGE_EN_DL="${BASE_URL}/entity-fishing/${_DATE}/db-en.zip"
KNOWLEDGE_ES_DL="${BASE_URL}/entity-fishing/${_DATE}/db-es.zip"
KNOWLEDGE_FA_DL="${BASE_URL}/entity-fishing/${_DATE}/db-fa.zip"
KNOWLEDGE_FR_DL="${BASE_URL}/entity-fishing/${_DATE}/db-fr.zip"
KNOWLEDGE_IN_DL="${BASE_URL}/entity-fishing/${_DATE}/db-hi.zip"
KNOWLEDGE_IT_DL="${BASE_URL}/entity-fishing/${_DATE}/db-it.zip"
KNOWLEDGE_JA_DL="${BASE_URL}/entity-fishing/${_DATE}/db-ja.zip"
KNOWLEDGE_PT_DL="${BASE_URL}/entity-fishing/${_DATE}/db-pt.zip"
KNOWLEDGE_RU_DL="${BASE_URL}/entity-fishing/${_DATE}/db-ru.zip"
KNOWLEDGE_SE_DL="${BASE_URL}/entity-fishing/${_DATE}/db-sv.zip"
KNOWLEDGE_UA_DL="${BASE_URL}/entity-fishing/${_DATE}/db-uk.zip"
KNOWLEDGE_ZH_DL="${BASE_URL}/entity-fishing/${_DATE}/db-zh.zip"
KNOWLEDGE_KB_DL="${BASE_URL}/entity-fishing/${_DATE}/db-kb.zip"
################################################################

################################################################
# Verbose output
GDATE_START=$( date +%s )
echo "[$(date)]: Starting download of entity-fishing prerequisites"
echo "[$(date)]: Downloading from URL ${BASE_URL}"
if [[ $_LANGS == *"ar"* ]] || [[ $_LANGS == *"Ar"* ]] || [[ $_LANGS == *"AR"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_EF_AR=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download entity-fishing AR language prerequisites"
  else
    echo " - You asked to download entity-fishing AR language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"bd"* ]] || [[ $_LANGS == *"Bd"* ]] || [[ $_LANGS == *"BD"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_EF_BD=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download entity-fishing BD language prerequisites"
  else
    echo " - You asked to download entity-fishing BD language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"de"* ]] || [[ $_LANGS == *"De"* ]] || [[ $_LANGS == *"DE"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_EF_DE=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download entity-fishing DE language prerequisites"
  else
    echo " - You asked to download entity-fishing DE language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"en"* ]] || [[ $_LANGS == *"En"* ]] || [[ $_LANGS == *"EN"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_EF_EN=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download entity-fishing EN language prerequisites"
  else
    echo " - You asked to download entity-fishing EN language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"es"* ]] || [[ $_LANGS == *"Es"* ]] || [[ $_LANGS == *"ES"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_EF_ES=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download entity-fishing ES language prerequisites"
  else
    echo " - You asked to download entity-fishing ES language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"fa"* ]] || [[ $_LANGS == *"Fa"* ]] || [[ $_LANGS == *"FA"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_EF_FA=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download entity-fishing FA language prerequisites"
  else
    echo " - You asked to download entity-fishing FA language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"fr"* ]] || [[ $_LANGS == *"Fr"* ]] || [[ $_LANGS == *"FR"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_EF_FR=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download entity-fishing FR language prerequisites"
  else
    echo " - You asked to download entity-fishing FR language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"in"* ]] || [[ $_LANGS == *"In"* ]] || [[ $_LANGS == *"IN"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_EF_IN=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download entity-fishing IN language prerequisites"
  else
    echo " - You asked to download entity-fishing IN language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"it"* ]] || [[ $_LANGS == *"It"* ]] || [[ $_LANGS == *"IT"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_EF_IT=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download entity-fishing IT language prerequisites"
  else
    echo " - You asked to download entity-fishing IT language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"ja"* ]] || [[ $_LANGS == *"Ja"* ]] || [[ $_LANGS == *"JA"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_EF_JA=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download entity-fishing JA language prerequisites"
  else
    echo " - You asked to download entity-fishing JA language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"pt"* ]] || [[ $_LANGS == *"Pt"* ]] || [[ $_LANGS == *"PT"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_EF_PT=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download entity-fishing PT language prerequisites"
  else
    echo " - You asked to download entity-fishing PT language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"ru"* ]] || [[ $_LANGS == *"Ru"* ]] || [[ $_LANGS == *"RU"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_EF_RU=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download entity-fishing RU language prerequisites"
  else
    echo " - You asked to download entity-fishing RU language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"se"* ]] || [[ $_LANGS == *"Se"* ]] || [[ $_LANGS == *"SE"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_EF_SE=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download entity-fishing SE language prerequisites"
  else
    echo " - You asked to download entity-fishing SE language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"ua"* ]] || [[ $_LANGS == *"Ua"* ]] || [[ $_LANGS == *"UA"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_EF_UA=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download entity-fishing UA language prerequisites"
  else
    echo " - You asked to download entity-fishing UA language prerequisites, dated $_DATE"
  fi
fi
if [[ $_LANGS == *"zh"* ]] || [[ $_LANGS == *"Zh"* ]] || [[ $_LANGS == *"ZH"* ]] || [[ $_LANGS == *"all"* ]] || [[ $_LANGS == *"ALL"* ]] ; then
  DOWNLOAD_EF_ZH=true
  if [ -z "$_DATE" ] ; then
    echo " - You asked to download entity-fishing ZH language prerequisites"
  else
    echo " - You asked to download entity-fishing ZH language prerequisites, dated $_DATE"
  fi
fi
################################################################

#-------- AR KNOWLEDGE --------
if [ "${DOWNLOAD_EF_AR}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of entity-fishing knowledge AR prerequisites"
  if [ ! -d "${PATH_EF_DATA}" ] ; then
    mkdir -p "${PATH_EF_DATA}"
  fi
  FOLDER_SIZE=0
  if [ -d "${PATH_EF_DATA}"/"${KNOWLEDGE_AR_FOLDER}" ] ; then
    FOLDER_SIZE=$( du -s "${PATH_EF_DATA}"/"${KNOWLEDGE_AR_FOLDER}" | awk -F ' ' '{print $1}' )
  fi
  if [ ! -d "${PATH_EF_DATA}"/"${KNOWLEDGE_AR_FOLDER}" ] || [ "${FOLDER_SIZE}" -le 1000 ] ; then
    cd "${PATH_EF_DATA}"
    echo " --[$(date)]: |-Downloading and extracting entity-fishing prerequisites [${KNOWLEDGE_AR_DL}]"
    wget -qO- "${KNOWLEDGE_AR_DL}" | bsdtar --exclude "*/markupFull/*" --exclude "*/entityEmbeddings/*" --exclude "*/wordEmbeddings/*" -xf-
  else
    echo " --[$(date)]: |-entity-fishing knowledge AR prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of entity-fishing knowledge AR prerequisites [${ELAPSED}]"
fi
#-------- AR KNOWLEDGE --------

#-------- BD KNOWLEDGE --------
if [ "${DOWNLOAD_EF_BD}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of entity-fishing knowledge BD prerequisites"
  if [ ! -d "${PATH_EF_DATA}" ] ; then
    mkdir -p "${PATH_EF_DATA}"
  fi
  FOLDER_SIZE=0
  if [ -d "${PATH_EF_DATA}"/"${KNOWLEDGE_BD_FOLDER}" ] ; then
    FOLDER_SIZE=$( du -s "${PATH_EF_DATA}"/"${KNOWLEDGE_BD_FOLDER}" | awk -F ' ' '{print $1}' )
  fi
  if [ ! -d "${PATH_EF_DATA}"/"${KNOWLEDGE_BD_FOLDER}" ] || [ "${FOLDER_SIZE}" -le 1000 ] ; then
    cd "${PATH_EF_DATA}"
    echo " --[$(date)]: |-Downloading and extracting entity-fishing prerequisites [${KNOWLEDGE_BD_DL}]"
    wget -qO- "${KNOWLEDGE_BD_DL}" | bsdtar --exclude "*/markupFull/*" --exclude "*/entityEmbeddings/*" --exclude "*/wordEmbeddings/*" -xf-
  else
    echo " --[$(date)]: |-entity-fishing knowledge BD prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of entity-fishing knowledge BD prerequisites [${ELAPSED}]"
fi
#-------- BD KNOWLEDGE --------

#-------- DE KNOWLEDGE --------
if [ "${DOWNLOAD_EF_DE}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of entity-fishing knowledge DE prerequisites"
  if [ ! -d "${PATH_EF_DATA}" ] ; then
    mkdir -p "${PATH_EF_DATA}"
  fi
  FOLDER_SIZE=0
  if [ -d "${PATH_EF_DATA}"/"${KNOWLEDGE_DE_FOLDER}" ] ; then
    FOLDER_SIZE=$( du -s "${PATH_EF_DATA}"/"${KNOWLEDGE_DE_FOLDER}" | awk -F ' ' '{print $1}' )
  fi
  if [ ! -d "${PATH_EF_DATA}"/"${KNOWLEDGE_DE_FOLDER}" ] || [ "${FOLDER_SIZE}" -le 1000 ] ; then
    cd "${PATH_EF_DATA}"
    echo " --[$(date)]: |-Downloading and extracting entity-fishing prerequisites [${KNOWLEDGE_DE_DL}]"
    wget -qO- "${KNOWLEDGE_DE_DL}" | bsdtar --exclude "*/markupFull/*" --exclude "*/entityEmbeddings/*" --exclude "*/wordEmbeddings/*" -xf-
  else
    echo " --[$(date)]: |-entity-fishing knowledge DE prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of entity-fishing knowledge DE prerequisites [${ELAPSED}]"
fi
#-------- DE KNOWLEDGE --------

#-------- EN KNOWLEDGE --------
if [ "${DOWNLOAD_EF_EN}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of entity-fishing knowledge EN prerequisites"
  if [ ! -d "${PATH_EF_DATA}" ] ; then
    mkdir -p "${PATH_EF_DATA}"
  fi
  FOLDER_SIZE=0
  if [ -d "${PATH_EF_DATA}"/"${KNOWLEDGE_EN_FOLDER}" ] ; then
    FOLDER_SIZE=$( du -s "${PATH_EF_DATA}"/"${KNOWLEDGE_EN_FOLDER}" | awk -F ' ' '{print $1}' )
  fi
  if [ ! -d "${PATH_EF_DATA}"/"${KNOWLEDGE_EN_FOLDER}" ] || [ "${FOLDER_SIZE}" -le 1000 ] ; then
    cd "${PATH_EF_DATA}"
    echo " --[$(date)]: |-Downloading and extracting entity-fishing prerequisites [${KNOWLEDGE_EN_DL}]"
    wget -qO- "${KNOWLEDGE_EN_DL}" | bsdtar --exclude "*/markupFull/*" --exclude "*/entityEmbeddings/*" --exclude "*/wordEmbeddings/*" -xf-
  else
    echo " --[$(date)]: |-entity-fishing knowledge EN prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of entity-fishing knowledge EN prerequisites [${ELAPSED}]"
fi
#-------- EN KNOWLEDGE --------

#-------- ES KNOWLEDGE --------
if [ "${DOWNLOAD_EF_ES}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of entity-fishing knowledge ES prerequisites"
  if [ ! -d "${PATH_EF_DATA}" ] ; then
    mkdir -p "${PATH_EF_DATA}"
  fi
  FOLDER_SIZE=0
  if [ -d "${PATH_EF_DATA}"/"${KNOWLEDGE_ES_FOLDER}" ] ; then
    FOLDER_SIZE=$( du -s "${PATH_EF_DATA}"/"${KNOWLEDGE_ES_FOLDER}" | awk -F ' ' '{print $1}' )
  fi
  if [ ! -d "${PATH_EF_DATA}"/"${KNOWLEDGE_ES_FOLDER}" ] || [ "${FOLDER_SIZE}" -le 1000 ] ; then
    cd "${PATH_EF_DATA}"
    echo " --[$(date)]: |-Downloading and extracting entity-fishing prerequisites [${KNOWLEDGE_ES_DL}]"
    wget -qO- "${KNOWLEDGE_ES_DL}" | bsdtar --exclude "*/markupFull/*" --exclude "*/entityEmbeddings/*" --exclude "*/wordEmbeddings/*" -xf-
  else
    echo " --[$(date)]: |-entity-fishing knowledge ES prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of entity-fishing knowledge ES prerequisites [${ELAPSED}]"
fi
#-------- ES KNOWLEDGE --------

#-------- FA KNOWLEDGE --------
if [ "${DOWNLOAD_EF_FA}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of entity-fishing knowledge FA prerequisites"
  if [ ! -d "${PATH_EF_DATA}" ] ; then
    mkdir -p "${PATH_EF_DATA}"
  fi
  FOLDER_SIZE=0
  if [ -d "${PATH_EF_DATA}"/"${KNOWLEDGE_FA_FOLDER}" ] ; then
    FOLDER_SIZE=$( du -s "${PATH_EF_DATA}"/"${KNOWLEDGE_FA_FOLDER}" | awk -F ' ' '{print $1}' )
  fi
  if [ ! -d "${PATH_EF_DATA}"/"${KNOWLEDGE_FA_FOLDER}" ] || [ "${FOLDER_SIZE}" -le 1000 ] ; then
    cd "${PATH_EF_DATA}"
    echo " --[$(date)]: |-Downloading and extracting entity-fishing prerequisites [${KNOWLEDGE_FA_DL}]"
    wget -qO- "${KNOWLEDGE_FA_DL}" | bsdtar --exclude "*/markupFull/*" --exclude "*/entityEmbeddings/*" --exclude "*/wordEmbeddings/*" -xf-
  else
    echo " --[$(date)]: |-entity-fishing knowledge FA prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of entity-fishing knowledge FA prerequisites [${ELAPSED}]"
fi
#-------- FA KNOWLEDGE --------

#-------- FR KNOWLEDGE --------
if [ "${DOWNLOAD_EF_FR}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of entity-fishing knowledge FR prerequisites"
  if [ ! -d "${PATH_EF_DATA}" ] ; then
    mkdir -p "${PATH_EF_DATA}"
  fi
  FOLDER_SIZE=0
  if [ -d "${PATH_EF_DATA}"/"${KNOWLEDGE_FR_FOLDER}" ] ; then
    FOLDER_SIZE=$( du -s "${PATH_EF_DATA}"/"${KNOWLEDGE_FR_FOLDER}" | awk -F ' ' '{print $1}' )
  fi
  if [ ! -d "${PATH_EF_DATA}"/"${KNOWLEDGE_FR_FOLDER}" ] || [ "${FOLDER_SIZE}" -le 1000 ] ; then
    cd "${PATH_EF_DATA}"
    echo " --[$(date)]: |-Downloading and extracting entity-fishing prerequisites [${KNOWLEDGE_FR_DL}]"
    wget -qO- "${KNOWLEDGE_FR_DL}" | bsdtar --exclude "*/markupFull/*" --exclude "*/entityEmbeddings/*" --exclude "*/wordEmbeddings/*" -xf-
  else
    echo " --[$(date)]: |-entity-fishing knowledge FR prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of entity-fishing knowledge FR prerequisites [${ELAPSED}]"
fi
#-------- FR KNOWLEDGE --------

#-------- IN KNOWLEDGE --------
if [ "${DOWNLOAD_EF_IN}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of entity-fishing knowledge IN prerequisites"
  if [ ! -d "${PATH_EF_DATA}" ] ; then
    mkdir -p "${PATH_EF_DATA}"
  fi
  FOLDER_SIZE=0
  if [ -d "${PATH_EF_DATA}"/"${KNOWLEDGE_IN_FOLDER}" ] ; then
    FOLDER_SIZE=$( du -s "${PATH_EF_DATA}"/"${KNOWLEDGE_IN_FOLDER}" | awk -F ' ' '{print $1}' )
  fi
  if [ ! -d "${PATH_EF_DATA}"/"${KNOWLEDGE_IN_FOLDER}" ] || [ "${FOLDER_SIZE}" -le 1000 ] ; then
    cd "${PATH_EF_DATA}"
    echo " --[$(date)]: |-Downloading and extracting entity-fishing prerequisites [${KNOWLEDGE_IN_DL}]"
    wget -qO- "${KNOWLEDGE_IN_DL}" | bsdtar --exclude "*/markupFull/*" --exclude "*/entityEmbeddings/*" --exclude "*/wordEmbeddings/*" -xf-
  else
    echo " --[$(date)]: |-entity-fishing knowledge IN prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of entity-fishing knowledge IN prerequisites [${ELAPSED}]"
fi
#-------- IN KNOWLEDGE --------

#-------- IT KNOWLEDGE --------
if [ "${DOWNLOAD_EF_IT}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of entity-fishing knowledge IT prerequisites"
  if [ ! -d "${PATH_EF_DATA}" ] ; then
    mkdir -p "${PATH_EF_DATA}"
  fi
  FOLDER_SIZE=0
  if [ -d "${PATH_EF_DATA}"/"${KNOWLEDGE_IT_FOLDER}" ] ; then
    FOLDER_SIZE=$( du -s "${PATH_EF_DATA}"/"${KNOWLEDGE_IT_FOLDER}" | awk -F ' ' '{print $1}' )
  fi
  if [ ! -d "${PATH_EF_DATA}"/"${KNOWLEDGE_IT_FOLDER}" ] || [ "${FOLDER_SIZE}" -le 1000 ] ; then
    cd "${PATH_EF_DATA}"
    echo " --[$(date)]: |-Downloading and extracting entity-fishing prerequisites [${KNOWLEDGE_IT_DL}]"
    wget -qO- "${KNOWLEDGE_IT_DL}" | bsdtar --exclude "*/markupFull/*" --exclude "*/entityEmbeddings/*" --exclude "*/wordEmbeddings/*" -xf-
  else
    echo " --[$(date)]: |-entity-fishing knowledge IT prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of entity-fishing knowledge IT prerequisites [${ELAPSED}]"
fi
#-------- IT KNOWLEDGE --------

#-------- JA KNOWLEDGE --------
if [ "${DOWNLOAD_EF_JA}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of entity-fishing knowledge JA prerequisites"
  if [ ! -d "${PATH_EF_DATA}" ] ; then
    mkdir -p "${PATH_EF_DATA}"
  fi
  FOLDER_SIZE=0
  if [ -d "${PATH_EF_DATA}"/"${KNOWLEDGE_JA_FOLDER}" ] ; then
    FOLDER_SIZE=$( du -s "${PATH_EF_DATA}"/"${KNOWLEDGE_JA_FOLDER}" | awk -F ' ' '{print $1}' )
  fi
  if [ ! -d "${PATH_EF_DATA}"/"${KNOWLEDGE_JA_FOLDER}" ] || [ "${FOLDER_SIZE}" -le 1000 ] ; then
    cd "${PATH_EF_DATA}"
    echo " --[$(date)]: |-Downloading and extracting entity-fishing prerequisites [${KNOWLEDGE_JA_DL}]"
    wget -qO- "${KNOWLEDGE_JA_DL}" | bsdtar --exclude "*/markupFull/*" --exclude "*/entityEmbeddings/*" --exclude "*/wordEmbeddings/*" -xf-
  else
    echo " --[$(date)]: |-entity-fishing knowledge JA prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of entity-fishing knowledge JA prerequisites [${ELAPSED}]"
fi
#-------- JA KNOWLEDGE --------

#-------- PT KNOWLEDGE --------
if [ "${DOWNLOAD_EF_PT}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of entity-fishing knowledge PT prerequisites"
  if [ ! -d "${PATH_EF_DATA}" ] ; then
    mkdir -p "${PATH_EF_DATA}"
  fi
  FOLDER_SIZE=0
  if [ -d "${PATH_EF_DATA}"/"${KNOWLEDGE_PT_FOLDER}" ] ; then
    FOLDER_SIZE=$( du -s "${PATH_EF_DATA}"/"${KNOWLEDGE_PT_FOLDER}" | awk -F ' ' '{print $1}' )
  fi
  if [ ! -d "${PATH_EF_DATA}"/"${KNOWLEDGE_PT_FOLDER}" ] || [ "${FOLDER_SIZE}" -le 1000 ] ; then
    cd "${PATH_EF_DATA}"
    echo " --[$(date)]: |-Downloading and extracting entity-fishing prerequisites [${KNOWLEDGE_PT_DL}]"
    wget -qO- "${KNOWLEDGE_PT_DL}" | bsdtar --exclude "*/markupFull/*" --exclude "*/entityEmbeddings/*" --exclude "*/wordEmbeddings/*" -xf-
  else
    echo " --[$(date)]: |-entity-fishing knowledge PT prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of entity-fishing knowledge PT prerequisites [${ELAPSED}]"
fi
#-------- PT KNOWLEDGE --------

#-------- RU KNOWLEDGE --------
if [ "${DOWNLOAD_EF_RU}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of entity-fishing knowledge RU prerequisites"
  if [ ! -d "${PATH_EF_DATA}" ] ; then
    mkdir -p "${PATH_EF_DATA}"
  fi
  FOLDER_SIZE=0
  if [ -d "${PATH_EF_DATA}"/"${KNOWLEDGE_RU_FOLDER}" ] ; then
    FOLDER_SIZE=$( du -s "${PATH_EF_DATA}"/"${KNOWLEDGE_RU_FOLDER}" | awk -F ' ' '{print $1}' )
  fi
  if [ ! -d "${PATH_EF_DATA}"/"${KNOWLEDGE_RU_FOLDER}" ] || [ "${FOLDER_SIZE}" -le 1000 ] ; then
    cd "${PATH_EF_DATA}"
    echo " --[$(date)]: |-Downloading and extracting entity-fishing prerequisites [${KNOWLEDGE_RU_DL}]"
    wget -qO- "${KNOWLEDGE_RU_DL}" | bsdtar --exclude "*/markupFull/*" --exclude "*/entityEmbeddings/*" --exclude "*/wordEmbeddings/*" -xf-
  else
    echo " --[$(date)]: |-entity-fishing knowledge RU prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of entity-fishing knowledge RU prerequisites [${ELAPSED}]"
fi
#-------- RU KNOWLEDGE --------

#-------- SE KNOWLEDGE --------
if [ "${DOWNLOAD_EF_SE}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of entity-fishing knowledge SE prerequisites"
  if [ ! -d "${PATH_EF_DATA}" ] ; then
    mkdir -p "${PATH_EF_DATA}"
  fi
  FOLDER_SIZE=0
  if [ -d "${PATH_EF_DATA}"/"${KNOWLEDGE_SE_FOLDER}" ] ; then
    FOLDER_SIZE=$( du -s "${PATH_EF_DATA}"/"${KNOWLEDGE_SE_FOLDER}" | awk -F ' ' '{print $1}' )
  fi
  if [ ! -d "${PATH_EF_DATA}"/"${KNOWLEDGE_SE_FOLDER}" ] || [ "${FOLDER_SIZE}" -le 1000 ] ; then
    cd "${PATH_EF_DATA}"
    echo " --[$(date)]: |-Downloading and extracting entity-fishing prerequisites [${KNOWLEDGE_SE_DL}]"
    wget -qO- "${KNOWLEDGE_SE_DL}" | bsdtar --exclude "*/markupFull/*" --exclude "*/entityEmbeddings/*" --exclude "*/wordEmbeddings/*" -xf-
  else
    echo " --[$(date)]: |-entity-fishing knowledge SE prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of entity-fishing knowledge SE prerequisites [${ELAPSED}]"
fi
#-------- SE KNOWLEDGE --------

#-------- UA KNOWLEDGE --------
if [ "${DOWNLOAD_EF_UA}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of entity-fishing knowledge UA prerequisites"
  if [ ! -d "${PATH_EF_DATA}" ] ; then
    mkdir -p "${PATH_EF_DATA}"
  fi
  FOLDER_SIZE=0
  if [ -d "${PATH_EF_DATA}"/"${KNOWLEDGE_UA_FOLDER}" ] ; then
    FOLDER_SIZE=$( du -s "${PATH_EF_DATA}"/"${KNOWLEDGE_UA_FOLDER}" | awk -F ' ' '{print $1}' )
  fi
  if [ ! -d "${PATH_EF_DATA}"/"${KNOWLEDGE_UA_FOLDER}" ] || [ "${FOLDER_SIZE}" -le 1000 ] ; then
    cd "${PATH_EF_DATA}"
    echo " --[$(date)]: |-Downloading and extracting entity-fishing prerequisites [${KNOWLEDGE_UA_DL}]"
    wget -qO- "${KNOWLEDGE_UA_DL}" | bsdtar --exclude "*/markupFull/*" --exclude "*/entityEmbeddings/*" --exclude "*/wordEmbeddings/*" -xf-
  else
    echo " --[$(date)]: |-entity-fishing knowledge UA prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of entity-fishing knowledge UA prerequisites [${ELAPSED}]"
fi
#-------- UA KNOWLEDGE --------

#-------- ZH KNOWLEDGE --------
if [ "${DOWNLOAD_EF_ZH}" == "true" ] ; then
  DATE_START=$( date +%s )
  echo " --[$(date)]: Starting deployment of entity-fishing knowledge ZH prerequisites"
  if [ ! -d "${PATH_EF_DATA}" ] ; then
    mkdir -p "${PATH_EF_DATA}"
  fi
  FOLDER_SIZE=0
  if [ -d "${PATH_EF_DATA}"/"${KNOWLEDGE_ZH_FOLDER}" ] ; then
    FOLDER_SIZE=$( du -s "${PATH_EF_DATA}"/"${KNOWLEDGE_ZH_FOLDER}" | awk -F ' ' '{print $1}' )
  fi
  if [ ! -d "${PATH_EF_DATA}"/"${KNOWLEDGE_ZH_FOLDER}" ] || [ "${FOLDER_SIZE}" -le 1000 ] ; then
    cd "${PATH_EF_DATA}"
    echo " --[$(date)]: |-Downloading and extracting entity-fishing prerequisites [${KNOWLEDGE_ZH_DL}]"
    wget -qO- "${KNOWLEDGE_ZH_DL}" | bsdtar --exclude "*/markupFull/*" --exclude "*/entityEmbeddings/*" --exclude "*/wordEmbeddings/*" -xf-
  else
    echo " --[$(date)]: |-entity-fishing knowledge ZH prerequisite already satisfied"
  fi
  DATE_END=$( date +%s )
  DIFF_SECONDS=$(( DATE_END - DATE_START ))
  ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
  echo " --[$(date)]: Ending deployment of entity-fishing knowledge ZH prerequisites [${ELAPSED}]"
fi
#-------- ZH KNOWLEDGE --------

#-------- KB KNOWLEDGE --------
DATE_START=$( date +%s )
echo " --[$(date)]: Starting deployment of entity-fishing knowledge KB prerequisites"
if [ ! -d "${PATH_EF_DATA}" ] ; then
  mkdir -p "${PATH_EF_DATA}"
fi
FOLDER_SIZE=0
if [ -d "${PATH_EF_DATA}"/"${KNOWLEDGE_KB_FOLDER}" ] ; then
  FOLDER_SIZE=$( du -s "${PATH_EF_DATA}"/"${KNOWLEDGE_KB_FOLDER}" | awk -F ' ' '{print $1}' )
fi
if [ ! -d "${PATH_EF_DATA}"/"${KNOWLEDGE_KB_FOLDER}" ] || [ "${FOLDER_SIZE}" -le 1000 ] ; then
  cd "${PATH_EF_DATA}"
  echo " --[$(date)]: |-Downloading and extracting entity-fishing prerequisites [${KNOWLEDGE_KB_DL}]"
  wget -qO- "${KNOWLEDGE_KB_DL}" | bsdtar --exclude "*/markupFull/*" --exclude "*/entityEmbeddings/*" --exclude "*/wordEmbeddings/*" -xf-
else
  echo " --[$(date)]: |-entity-fishing knowledge KB prerequisite already satisfied"
fi
DATE_END=$( date +%s )
DIFF_SECONDS=$(( DATE_END - DATE_START ))
ELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
echo " --[$(date)]: Ending deployment of entity-fishing knowledge KB prerequisites [${ELAPSED}]"
#-------- KB KNOWLEDGE --------

# Give permissions to workdir
chown -R kairntech:kairntech "${PATH_EF_DATA}"

GDATE_END=$( date +%s )
DIFF_SECONDS=$(( GDATE_END - GDATE_START ))
GELAPSED="$((DIFF_SECONDS/3600))h $(((DIFF_SECONDS/60)%60))m $((DIFF_SECONDS%60))s"
echo "[$(date)]: Ending download of entity-fishing prerequisites [${GELAPSED}]"
