# KAIRNTECH Products Delivery Area #

The purpose of this repository is to offer access to Kairntech Products.

A generic version of Installation steps is described here.
<br>

## Overview ##
All listed commands below come from the environment <span style="color: cyan;"> **UBUNTU 18.04 LTS x64** </span> and consists in:

   * preparing the instance
      * adapting `sysctl` settings on the host ([Prerequisites host configuration](README.md#markdown-header-prerequisites-host-configuration))
      * creating a user and folders to install the platform ([User/Folders creation](README.md#markdown-header-userfolders-creation))
      * installing the required binaries docker and docker compose ([Binaries installation](README.md#markdown-header-binaries-installation))

   * deploying the platform 
      * mount Docker volumes and fill with static files ([Docker volumes to mount](README.md#markdown-header-docker-volumes-to-mount))
	  * pull and start the platform ([Kairntech platform installation](README.md#markdown-header-kairntech-platform-installation))
<br>

## Prerequisites host configuration ##
### ELASTIC SEARCH recommendation ###
You may need to increase the `vm.max_map_count` kernel parameter to avoid running out of map areas.<br>
In order to avoid such message
```sh
[1]: max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]
```

It is recommended to edit file `/etc/sysctl.conf` and insert the following lines:
```bash
# ES - at least 262144 for production use
vm.max_map_count=262144
```

In order to apply the modification, please run
```bash
sudo sysctl -p
```
<br>

### INOTIFY recommendation ###
You may need to increase the `fs.inotify.max_user_instances` parameter to avoid reaching user limits on the number of inotify resources.<br>
In order to avoid such message
```sh
[Errno 24] inotify instance limit reached
```

It is recommended to edit file `/etc/sysctl.conf` and insert the following lines:
```bash
# Prevent [Errno 24] inotify instance limit reached
fs.inotify.max_user_instances = 65530
```

In order to apply the modification, please run
```bash
sudo sysctl -p
```
<br>

### HAPROXY recommendation ###
You may need to set `net.ipv4.ip_unprivileged_port_start` to let to non root user haproxy the permission to run on priviledged port 443.<br>
In order to avoid such message (in haproxy container console output)
```sh
[ALERT]    (1) : Starting frontend http-in-sherpa: cannot bind socket (Permission denied) [0.0.0.0:443]
[ALERT]    (1) : [haproxy.main()] Some protocols failed to start their listeners! Exiting.
```

It is recommended to edit file `/etc/sysctl.conf` and insert the following lines:
```bash
# Enable haproxy to listen to 443
net.ipv4.ip_unprivileged_port_start=0
```

In order to apply the modification, please run
```bash
sudo sysctl -p
```
<br><br>

## User/Folders creation ##
### USER creation ###
Is it highly advised to create a specific user, for the deployment of the platform:
```bash
# FOR A STANDARD USER
sudo adduser kairntech

# OR FOR A HEADLESS USER
sudo adduser --disabled-password --gecos "" kairntech
```
<br>

### FOLDERS creation ###
Is it highly advised to create specific folders, for the deployment of the platform:
```bash
sudo mkdir -p /opt/sherpa
sudo chown -R kairntech. /opt/sherpa

mkdir -p ~/embeddings
```
<br>

The content of the prepared folders will consist in:
<ul>
<li>
Directory `/opt/sherpa/` will store all files and folders relative to the platform <br>(**delivered by Kairntech**)
<ul>
<li>
File `docker-compose.yml` to be used to deploy/pull Docker images of the platform 
</li>
<li>
File `sherpa-core` to be used to store authentication mechanism keys and deploy specific components
</li>
<li>
File `sherpa-haproxy` to be used in case redirections are set (optionnal)
</li>
</ul>
</li>
<br/>
<li>
Directory `~/embeddings` will store all files required to feed Docker volumes <br>(**delivered by Kairntech**)
<br>
<ul>
<li>
File `deploy-embeddings-delft.sh` to be used to deploy Delft embeddings
</li>
<li>
File `docker-compose.delft.volumes.yml` also to be used to deploy Delft embeddings
</li>
<li>
File `deploy-embeddings-flair.sh` to be used to deploy Flair embeddings
</li>
<li>
File `docker-compose.flair.volumes.yml` also to be used to deploy Flair embeddings
</li>
<li>
File `deploy-embeddings-fasttext.sh` to be used to deploy fastText embeddings
</li>
<li>
File `docker-compose.fasttext.volumes.yml` also to be used to deploy fastText embeddings
</li>
<li>
File `deploy-knowledge-entityfishing.sh` to be used to deploy Entity-Fishing knowledge
</li>
<li>
File `docker-compose.ef.volumes.yml` also to be used to deploy Entity-Fishing knowledge
</li>
<li>
File `deploy-documentation-sherpa.sh` to be used to deploy offline documentation (optionnal)
</li>
<li>
File `docker-compose.doc.volumes.yml` also to be used to deploy offline documentation (optionnal)
</li>
</ul>
</li>
</ul>
<br><br>

## Binaries installation ##
### DOCKER installation ###
The platform being based on a Docker-type solution, please install docker.<br>
The official page indicating the installation commands is located at https://docs.docker.com/engine/install/ubuntu/.
```bash
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
 https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

Then you will have to add the kairntech user to the docker group
```bash
sudo usermod -aG docker kairntech
```

If you want to test, **open a new session terminal** and run:
```bash
sudo su - kairntech

docker run hello-world
```
<br>

### DOCKER COMPOSE PLUGIN installation ###
Please also install docker compose plugin.<br>
The official page indicating the installation commands is located at https://docs.docker.com/compose/install/linux/
```bash
sudo su
apt-get install docker-compose-plugin
```

After installing docker compose plugin, you can test via:
```bash
sudo su - kairntech

docker compose version
```
<br><br>

## Docker volumes to mount ##
### FLAIR embeddings ###
In order to fully utilize the Flair engine, « embeddings » files must be downloaded.<br>
These static files are stored as Docker volumes. In order to download these items, please run:
```bash
sudo su - kairntech

cd ~/embeddings

# INSTALL AR, DE, EN AND FR
export FLAIR_LANGS=ar,de,en,fr
docker compose -f docker-compose.flair.volumes.yml -p volumes-flair up

# OR INSTALL ALL LANGUAGES
export FLAIR_LANGS=all
docker compose -f docker-compose.flair.volumes.yml -p volumes-flair up
```
<br>

Once deployed, you should get the following sizes (all languages)
```bash
sudo du -hs /var/lib/docker/volumes/sherpashared_flair_suggester_datasets
8.0K

sudo du -hs /var/lib/docker/volumes/sherpashared_flair_suggester_embeddings
30GB
```
<br>

The Docker container can be removed, once Flair embeddings are deployed, via:
```bash
docker rm flair-suggester-init-job
```
<br>

The table below gives disk usage required to deploy available languages:
<table><tbody><tr>
<td>

| language | size |
| :------: | :-------- |
| ar | 2.6G |
| de | 4.0G |
| en | 5.2G |
| es | 3.9G |
| fr | 3.9G |

</td>
<td>&#x0020; &#x0020;</td>
<td>

| language | &#x0020;size |
| :------: | :-------- |
| hi | 600M |
| it | 3.6G |
| nl | 3.6G |
| zh | 2.3G |
| multi | 330M |

</td>
<td>&#x0020; &#x0020;</td>
<td>

Total : <span style="color: yellow;"> **30G** </span>

</td>
</tr></tbody></table>
<br>

### DELFT embeddings ###
In order to fully utilize the Delft engine, « embeddings » files must be downloaded.<br>
These static files are stored as Docker volumes. In order to download these items, please run:
```bash
sudo su - kairntech

cd ~/embeddings

# INSTALL DE, EN AND FR
export DELFT_LANGS=de,en,fr
docker compose -f docker-compose.delft.volumes.yml -p volumes-delft up

# OR INSTALL ALL LANGUAGES
export DELFT_LANGS=all
docker compose -f docker-compose.delft.volumes.yml -p volumes-delft up
```
<br>

Once deployed, you should get the following sizes (all languages)
```bash
sudo du -hs /var/lib/docker/volumes/sherpashared_delft_suggester_database/
22G	/var/lib/docker/volumes/sherpashared_delft_suggester_database/

sudo du -hs /var/lib/docker/volumes/sherpashared_delft_suggester_embeddings/
2.2G	/var/lib/docker/volumes/sherpashared_delft_suggester_embeddings/
```
<br>

The Docker container can be removed, once Delft embeddings are deployed, via:
```bash
docker rm delft-suggester-init-job
```
<br>

The table below gives disk usage required to deploy available languages:
<table><tbody><tr>
<td>

| language | &#x0020;size |
| :------: | :-------- |
| de | 6.3G |
| en | 7.2G |
| es | 2.6G |

</td>
<td>&#x0020; &#x0020;</td>
<td>

| language | &#x0020;size |
| :------: | :-------- |
| fr | 3.6G |
| it | 2.3G |
| nl | 2.2G |

</td>
<td>&#x0020; &#x0020;</td>
<td>

Total : <span style="color: yellow;"> **24.2G** </span>

</td>
</tr></tbody></table>
<br>

### FASTTEXT embeddings ###
In order to fully utilize the fastText engine, « embeddings » files must be downloaded.<br>
These static files are stored as Docker volumes. In order to download these items, please run:
```bash
sudo su - kairntech

cd ~/embeddings

# INSTALL AR, DE, EN AND FR
export FASTTEXT_LANGS=ar,de,en,fr
docker compose -f docker-compose.fasttext.volumes.yml -p volumes-fasttext up

# OR INSTALL ALL LANGUAGES
export FASTTEXT_LANGS=all
docker compose -f docker-compose.fasttext.volumes.yml -p volumes-fasttext up
```
<br>

Once deployed, you should get the following sizes (all languages)
```bash
sudo du -hs /var/lib/docker/volumes/sherpashared_fasttext_suggester_embeddings
30GB
```
<br>

The Docker container can be removed, once fastText embeddings are deployed, via:
```bash
docker rm fasttext-suggester-init-job
```
<br>

The table below gives disk usage required to deploy available languages:
<table><tbody><tr>
<td>

| language | &#x0020;size |
| :------: | :-------- |
| ar | 1.5G |
| de | 5.6G |
| en | 6.2G |
| es | 2.5G |
| fr | 2.9G |

</td>
<td>&#x0020; &#x0020;</td>
<td>

| language | &#x0020;size |
| :------: | :-------- |
| it | 2.2G |
| ja | 1.3G |
| pt | 1.5G |
| ru | 4.7G |
| zh | 822M |

</td>
<td>&#x0020; &#x0020;</td>
<td>

Total : <span style="color: yellow;"> **30G** </span>

</td>
</tr></tbody></table>
<br>

### ENTITY-FISHING knowledge ###
In order to fully utilize the Entity-Fishing engine, « knowledge » files must be downloaded.<br>
These static files are generated every month, and stored as Docker volumes. In order to download these items, please run:
```bash
sudo su - kairntech

cd ~/embeddings

# INSTALL AR, DE, EN AND FR - GENERATED ON 02-03-2023
export EF_LANGS=ar,de,en,fr
export EF_DATE=02-03-2023
docker compose -f docker-compose.ef.volumes.yml -p volumes-ef up

# OR INSTALL ALL LANGUAGES - GENERATED ON 02-03-2023
export EF_LANGS=all
export EF_DATE=02-03-2023
docker compose -f docker-compose.ef.volumes.yml -p volumes-ef up
```
<br>

Once deployed, you should get the following sizes (all languages)
```bash
sudo du -hs /var/lib/docker/volumes/sherpa_entityfishing_data
101GB
```
<br>

The Docker container can be removed, once entity-fishing knowledge is deployed, via:
```bash
docker rm entity-fishing-init-job
```
<br>

The table below gives disk usage required to deploy available languages:
<table><tbody><tr>
<td>

| language | &#x0020;size | &#x0020;&#x0020;&#x0020;detail |
| :------: | :-------- | :-------- |
| ar | 37G | 33G + 3.7G |
| de | 39G | 33G + 6.0G |
| en | 49G | 33G + 16G |
| es | 37G | 33G + 4.4G |
| fa | 37G | 33G + 3.5G |
| fr | 39G | 33G + 5.6G |
| it | 37G | 33G + 3.9G |

</td>
<td>&#x0020; &#x0020;</td>
<td>

| language | &#x0020;size | &#x0020;&#x0020;&#x0020;detail |
| :------: | :-------- | :-------- |
| ja | 37G | 33G + 3.6G |
| pt | 36G | 33G + 2.8G |
| ru | 40G | 33G + 6.4G |
| ua | 37G | 33G + 3.6G |
| se | 37G | 33G + 4.2G |
| zh | 36G | 33G + 3.1G |

</td>
</tr></tbody></table>

Total : <span style="color: yellow;"> **100G** </span> <br>
<br>

### DOCUMENTATION (optionnal) ###
In order to benefit a documentation update, corresponding files must be downloaded.<br>
These prerequisite files are stored as Docker volumes. In order to download these items, please run:
```bash
sudo su - kairntech

cd ~/embeddings

# INSTALL VERSION DATED 10/27/2022
export DOC_VERSION='2022.10.27'
docker compose -f docker-compose.doc.volumes.yml -p documentation-sherpa up
```
<br>

Once deployed, you should get the following sizes (all languages)
```bash
sudo du -hs /var/lib/docker/volumes/sherpashared_sherpa-doc/_data/
21M    /var/lib/docker/volumes/sherpashared_sherpa-doc/_data/
```
<br>

The Docker container can be removed, once documentation is deployed, via:
```bash
docker rm documentation-sherpa-init-job
```
<br>

When updating embedded documentation, a specific volume must be added to the `sherpa-core` volumes section:
```bash
#########################################################################
# SHERPA CORE
    sherpa-core:
    ...
    volumes:
        ...
        - sherpa-doc:/app/kairntech/sherpa/doc
```
<br>

With its corresponding volume declaration (at the end of the `docker-compose.yml` file):
```bash
#########################################################################
volumes:
    ...
    sherpa-doc:
      external: true
      name: sherpashared_sherpa-doc
```
<br><br>

## Kairntech platform installation ##
As a first step, in order to configure JWT authentication, run the following commands:
```bash
sudo su - kairntech

cd /opt/sherpa/sherpa-core/jwt

##  In order to generate private.pem
openssl genrsa -out private.pem 2048

##  In order to generate private_key.pem
openssl pkcs8 -topk8 -inform PEM -in private.pem -out private_key.pem -nocrypt

##  In order to generate public.pem
openssl rsa -in private.pem -outform PEM -pubout -out public.pem
```
<br>

This will generate 3 files:

   * `private.pem`, to be kept in a safe place
   * `private_key.pem`, to be used in `sherpa-core/jwt` folder
   * `public.pem`, to be used in `sherpa-core/jwt` folder

<br>

Then in order to download the different images, you must first connect to dockerhub.<br>
(The password <span style="color: green;"> **will be delivered by Kairntech** </span>).
```bash
sudo su - kairntech

cd /opt/sherpa

docker login

username: ktguestkt

password: 
```
<br>

Once logged in, you can start downloading the images:
```bash
docker compose -f docker-compose.yml pull
```
<br>

Finally, to start the platform, run:
```bash
docker compose -f docker-compose.yml up -d
```
<br>

You can check the status of the containers; the following console output is given as an example. <br>
Some containers may not be present, depending on the kind of deployment you processed. 
```bash
docker ps -a --format "{{.ID}}\t\t{{.Names}}\t\t{{.Status}}"

79e235f82787        sherpa-core                           Up 20 sec

e69f95855809        sherpa-crfsuite-suggester             Up 20 sec
c9d95639c808        sherpa-entityfishing-suggester        Up 20 sec
94e4574b95de        sherpa-fasttext-suggester             Up 20 sec

8f13e72aeb0d        sherpa-phrasematcher-test-suggester   Up 20 sec
0f49dec91340        sherpa-phrasematcher-train-suggester  Up 20 sec
aa08f1008770        sherpa-sklearn-test-suggester         Up 20 sec
988976ef327d        sherpa-sklearn-train-suggester        Up 20 sec
bed6169d9185        sherpa-spacy-test-suggester           Up 20 sec
302bd98a44ab        sherpa-spacy-train-suggester          Up 20 sec
7754162ae44c        sherpa-flair-test-suggester           Up 20 sec
08d1ad415adb        sherpa-flair-train-suggester          Up 20 sec
8ded96094605        sherpa-delft-test-suggester           Up 20 sec
ebe47bd3ddf3        sherpa-delft-train-suggester          Up 20 sec
4835129a77c9        sherpa-bertopic-test-suggester        Up 20 sec
b999a848044c        sherpa-bertopic-train-suggester       Up 20 sec

0826e0dd9c85        sherpa-elasticsearch                  Up 20 sec
7f781bf11ddf        sherpa-mongodb                        Up 20 sec

d3b0e0557309        sherpa-builtins-importer              Up 20 sec

cf075d3b06f4        sherpa-multirole                      Up 20 sec
ae1b24e0ccdb        sherpa-pymultirole                    Up 20 sec
2a737b399388        sherpa-pymultirole-trf                Up 20 sec
f43121e96544        sherpa-pymultirole-ner                Up 20 sec
```
<br><br>

## Content of the delivery area ##

* [sherpa](sherpa): configuration files to be used to deploy sherpa studio version
* [sherpaprod](sherpaprod): configuration files to be used to deploy sherpa prod version 
* [entity-fishing](entity-fishing): configuration files to be used to deploy entity-fishing
* [grobid](grobid): configuration files to be used to deploy grobid
* [manager](manager): common script to play with products

---
### Content of sherpa delivery ###

* [crontab](sherpa/crontab)
	* sherpa-dumper.sh: script used to manage backups
	* sherpa-oom-watcher.sh: script used to watch OutOfMemory issues
	* sherpa-watcher.sh: script used to watch databases drop operations

* [soft](sherpa/soft)
	* docker-compose.yml: sherpa studio orchestration
	* sherpa-xxx configuration files and folders

* [volumes](sherpa/volumes)
	* docker-compose.delft.volumes.yml: delft embeddings deployment orchestration
	* docker-compose.flair.volumes.yml: flair embeddings deployment orchestration
	* docker-compose.fasttext.volumes.yml: fastText embeddings deployment orchestration
	* docker-compose.doc.volumes.yml: offline documentation deployment orchestration
	* deploy-embeddings-delft.sh: script used to deploy delft embeddings
	* deploy-embeddings-flair.sh: script used to deploy flair embeddings
	* deploy-embeddings-fasttext.sh: script used to deploy fastText embeddings
	* deploy-documentation-sherpa.sh: script used to deploy offline documentation

---
### Content of sherpaprod delivery ###

* [soft](sherpaprod/soft)
	* docker-compose.yml: sherpa prod orchestration
	* sherpa-xxx configuration files and folders

* [volumes](sherpaprod/volumes)
	* docker-compose.delft.volumes.yml: delft embeddings deployment orchestration
	* docker-compose.flair.volumes.yml: flair embeddings deployment orchestration
	* docker-compose.fasttext.volumes.yml: fastText embeddings deployment orchestration
	* deploy-embeddings-delft.sh: script used to deploy delft embeddings
	* deploy-embeddings-flair.sh: script used to deploy flair embeddings
	* deploy-embeddings-fasttext.sh: script used to deploy fastText embeddings

---
### Content of entity-fishing delivery ###

* [soft](entity-fishing/soft)
	* docker-compose.yml: Entity-Fishing orchestration
	* sherpa-haproxy: haproxy configuration files (and certificates)
	* warm-up: folder containing pre-recorded json requests to play to warm up
	* webapp: folder used to adapt interface of Entity-Fishing, kairntech copyright

* [volumes](entity-fishing/volumes)
	* docker-compose.ef.volumes.yml: entity-fishing knowledge deployment orchestration
	* deploy-knowledge-entityfishing.sh: script used to deploy entity-fishing knowledge

---
### Content of grobid delivery ###

* [soft](grobid/soft)
	* docker-compose.yml: grobid orchestration
	* sherpa-haproxy: haproxy configuration files (and certificates)

---
### Content of manager ###

* [dockermanager](manager/dockermanager.sh): script used to manage sherpa, entity-fishing, grobid products
